/* 
 * File:   MCTSTree.h
 * Author: Red
 *
 * Created on 20 de marzo de 2013, 20:20
 */

#ifndef MCTSTREE_H
#define	MCTSTREE_H

#include <vector>

typedef std::vector<int> VecI;

class MCTSTree {

public:

    MCTSTree();
    MCTSTree(const MCTSTree& orig);
    MCTSTree(MCTSTree* dad2, int i2, int j2, int col2, double v, int w, int t);
    virtual ~MCTSTree();
    int getBest();
    void updateSon(int i, int j, int res);
    void printTree(int sp);
    
    MCTSTree * dad;
    std::vector<MCTSTree*> sons;
    double value;
    int i;
    int j;
    int col;
    int win;
    int tot;
    
private:

};

#endif	/* MCTSTREE_H */

