/* 
 * File:   MCTSEngine.cpp
 * Author: Red
 * 
 * Created on 20 de marzo de 2013, 20:11
 */

#include "MCTSEngine.h"

MCTSEngine::MCTSEngine() {
}

MCTSEngine::MCTSEngine(const MCTSEngine& orig) {
}

MCTSEngine::~MCTSEngine() {
}

GoMove MCTSEngine::nextMove(GoBoard2& board, char color) {
    //std::ofstream flog;
    //flog.open("log.txt", std::ios::app);
    MCTSTree tree(0,0,0,(color+1)%2,0,0,0);
    int it = 3000;
    GoBoard2* b2;
    //board.printBoard();
    //board.printGroups();
    while(it--) {
        b2 = new GoBoard2(board);
        //b2->printBoard();
        //b2->printGroups();
        bool col = color;
        bool intree = true;
        MCTSTree* cont = &tree;
        int bestm;
        while (intree) {
            bestm = cont->getBest();
            if (bestm < 0 || cont->sons[bestm]->value < 0.5) intree = false;
            else {
                GoMove mov(cont->sons[bestm]->i, cont->sons[bestm]->j, 
                           cont->sons[bestm]->col,true);
                b2->nextMove(mov);
                cont = cont->sons[bestm];
                col = !col;
            }
        }
        //b2->printBoard();
        int move = b2->getRandomMove(col);
        //flog << "newrandomgame" << std::endl;
        int res = b2->randomGameMCTS(move, col);
        //flog << "updatesons" << std::endl;
        if (move >= 0) cont->updateSon(move/board.SIZE, move%board.SIZE, res);
        else cont->updateSon(-1, -1, res);
        //flog << "doneupdate" << std::endl;
        //tree.printTree(0);
        //b2->printBoard();
        //b2->printScore();
        delete b2;
    }
    tree.printTree(0);
    //flog << "getbest" << std::endl;
    int ind = tree.getBest();
    GoMove mov(tree.sons[ind]->i, tree.sons[ind]->j, color, true);
    if (tree.sons[ind]->value < 0.2) mov.valid = false;
    //flog << "doneall" << std::endl;
    return mov;
}

