/* 
 * File:   GoBoard3.h
 * Author: Red
 *
 * Created on 7 de abril de 2013, 20:25
 */

#ifndef GOBOARD3_H
#define	GOBOARD3_H
#include <stdlib.h>
#include <stdio.h>
#include <cstring>
#include "move.h"

#define FORBIDBONUS -1000
#define BASEBONUS 8

#define P_1LIBS 900
#define P_2LIBS 900
#define P_3LIBS 200
#define P_PATTERNS 900
#define P_SELFATARI 100
#define P_GCAPTURE 900
#define P_GATARI 200
#define P_CAPLADDER 800

#define MAXMOVES 300
#define LIBS_MAX 4

#define TAMBOARDSET 50

struct groupInfo {
    int libs;
    int coords[LIBS_MAX];
};

struct neighborsColor {
    char col[TYPE_MAX];
};



class GoBoard3 {
    
public:
    GoBoard3();
    GoBoard3(const GoBoard3& orig);
    virtual ~GoBoard3();
    
    int size;
    int size2;
    int * b; // Board and stones played (indexed by coord) |
    int * g; // Group id of every stone                    |  Similar to pachi
    int * p; // Position of next stone in the group        |
    int * f; // Free position in the board (all empty)     |
    int flen; // Number of free positions in the board     |
    groupInfo * ginfo; // Information about groups (libs)  | 
    neighborsColor * nscolor; // Counters of neighbors     |
    
    char * patternCache;
    
    int * zob;
    int * hashes;
    int hashlen;
    int hashact;
    bool superko;
    
    int * capturable;
    int caplen;
    int * atariable;
    int atalen;
    
    int * movs;
    int turns;
    
    int memsize; // size of the arrays in memory
    
    float komi;
    int ko;
    int koColor;
    int lastMove;
    int lastMove2;
    int capBlack;
    int capWhite;
    
    void resetStateBoard(const GoBoard3& orig);
    void allocBoard();
    void initBoard();
    void setSize(int siz);
    void printBoard();
    void printGroups();
    void printNsColor();
    void printStructures();
    void setKomi(float);
    int playHeuristicMove(int color);
    int playRandomMove(int color);
    int playMove(int c, int color);
    float playRandomGame(int color, double dynkomi);
    inline bool isLegalMove(int c, int color);
    bool checkNeighbors();
    float getScore(double dynkomi);
    int getBonus(int c, int color, int& wins, int& loses);
    bool isLadderp(int coord, int group, int color);
    inline bool canBeLadder(int group, int coord, int opponent, int color);
    
private:
    
    int moves[200]; // temporary array for playHeuristic
    char ladderCache[121];
    
    int playInCoord(int coord, int color, int fcoord);
    int playInEye(int coord, int color, int fcoord);
    inline void searchMoreLibs(int coord, int group);
    inline void addLibToGroup(int coord, int group);
    inline void remLibFromGroup(int coord, int group);
    int updateNeighbor(int orig_c, int coord, int color, 
                                 int group);
    void addToGroup(int orig_c, int coord, int group);
    int createGroup(int coord);
    void fusionGroups(int group1, int group2);
    int captureGroup(int group);
    void captureStone(int coord, int group);
    void remCapturable(int group);
    void remAtariable(int group);
    int countEscape(int coord, int color);
    inline bool isLadder(int coord, int group, int color);
    bool isBorderLadder(int coord, int color);
    bool isExpensiveLadder(int coord, int group, int color);
    bool isRecursiveLadder(GoBoard3& b2, int group, 
                              int next, int color);
    inline bool isFalseEye(int coord, int color);
    inline bool isSuicide(int coord, int color);
    inline bool isSelfAtari(int coord, int color);
    inline int getClosePos(int * moves, int color, int top, int start,
                             char * used);
    inline bool repeatHash();
    inline bool matchSomePattern(int coord, int color);
};

#define likely(x) __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect((x), 0)

#define isBlack(c) (b[c] == BLACK)
#define isWhite(c) (b[c] == WHITE)
#define isEmpty(c) (b[c] == EMPTY)
#define isBorder(c) (b[c] == BORDER)
#define isPass(c) (c == PASS)
#define isResign(c) (c == RESIGN)

#define isSurrounded(c, clr) (nscolor[c].col[clr] + nscolor[c].col[BORDER] == 4)
#define isCaptured(g) (ginfo[g].libs == 0)

#define for_each_point \
        for (int cp = 0; cp < size2; ++cp)

#define for_each_free_point \
        for (int c = 0; c < flen; ++c)

#define for_each_neighbor(_dec, _coord, _todo) \
        int _dec = _coord - size; \
        _todo \
        _dec = _coord - 1; \
        _todo \
        _dec = _coord + 1; \
        _todo \
        _dec = _coord + size; \
        _todo

#define for_each_diagonal(_dec, _coord, _todo) \
        int _dec = _coord - size - 1; \
        _todo \
        _dec = _coord - size + 1; \
        _todo \
        _dec = _coord + size - 1; \
        _todo \
        _dec = _coord + size + 1; \
        _todo

#define for_each_8neighbor(_dec, _coord, _todo) \
        int _dec = _coord - size - 1; \
        _todo \
        _dec = _coord - size; \
        _todo \
        _dec = _coord -size + 1; \
        _todo \
        _dec = _coord - 1; \
        _todo \
        _dec = _coord + 1; \
        _todo \
        _dec = _coord + size - 1; \
        _todo \
        _dec = _coord + size; \
        _todo \
        _dec = _coord + size + 1; \
        _todo

#define for_each_in_group(_group, _todo) \
        int c = _group; \
        do { \
            _todo \
            c = p[c]; \
        } while(c);

#define for_both_colors(_todo) \
        col1 = BLACK; \
        col2 = WHITE; \
        _todo \
        col1 = WHITE; \
        col2 = BLACK; \
        _todo \

#define check_pattern(_coord, _todo)    \
        int col1, col2;                 \
        int c1 = _coord - size - 1;     \
        int c2 = _coord - size;         \
        int c3 = _coord - size + 1;     \
        int c4 = _coord - 1;            \
        int c5 = _coord + 1;            \
        int c6 = _coord + size - 1;     \
        int c7 = _coord + size;         \
        int c8 = _coord + size + 1;     \
        for_both_colors(_todo)          \
        c1 = _coord - size + 1;         \
        c2 = _coord - size;             \
        c3 = _coord - size - 1;         \
        c4 = _coord - 1;                \
        c5 = _coord + 1;                \
        c6 = _coord + size + 1;         \
        c7 = _coord + size;             \
        c8 = _coord + size - 1;         \
        for_both_colors(_todo)          \
        c1 = _coord + size - 1;         \
        c2 = _coord + size;             \
        c3 = _coord + size + 1;         \
        c4 = _coord - 1;                \
        c5 = _coord + 1;                \
        c6 = _coord - size - 1;         \
        c7 = _coord - size;             \
        c8 = _coord - size + 1;         \
        for_both_colors(_todo)          \
        c1 = _coord + size + 1;         \
        c2 = _coord + size;             \
        c3 = _coord + size - 1;         \
        c4 = _coord + 1;                \
        c5 = _coord - 1;                \
        c6 = _coord - size + 1;         \
        c7 = _coord - size;             \
        c8 = _coord - size - 1;         \
        for_both_colors(_todo)          \
                                        \
        c1 = _coord - size + 1;         \
        c2 = _coord + 1;                \
        c3 = _coord + size + 1;         \
        c4 = _coord - size;             \
        c5 = _coord + size;             \
        c6 = _coord - size - 1;         \
        c7 = _coord - 1;                \
        c8 = _coord + size - 1;         \
        for_both_colors(_todo)          \
        c1 = _coord - size - 1;         \
        c2 = _coord - 1;                \
        c3 = _coord + size - 1;         \
        c4 = _coord - size;             \
        c5 = _coord + size;             \
        c6 = _coord - size + 1;         \
        c7 = _coord + 1;                \
        c8 = _coord + size + 1;         \
        for_both_colors(_todo)          \
        c1 = _coord + size + 1;         \
        c2 = _coord + 1;                \
        c3 = _coord - size + 1;         \
        c4 = _coord + size;             \
        c5 = _coord - size;             \
        c6 = _coord + size - 1;         \
        c7 = _coord - 1;                \
        c8 = _coord - size - 1;         \
        for_both_colors(_todo)          \
        c1 = _coord + size - 1;         \
        c2 = _coord - 1;                \
        c3 = _coord - size - 1;         \
        c4 = _coord + size;             \
        c5 = _coord - size;             \
        c6 = _coord + size + 1;         \
        c7 = _coord + 1;                \
        c8 = _coord - size + 1;         \
        for_both_colors(_todo)

#endif	/* GOBOARD3_H */

