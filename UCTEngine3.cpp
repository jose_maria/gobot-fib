/* 
 * File:   UCTEngine3.cpp
 * Author: Red
 * 
 * Created on 28 de abril de 2013, 20:42
 */

#include "UCTEngine3.h"

UCTEngine3::UCTEngine3() {
    resetTree = false;
    baseTree = 0;
}

UCTEngine3::UCTEngine3(const UCTEngine3& orig) {
}

UCTEngine3::~UCTEngine3() {
    if (baseTree) {
        eraseSons(baseTree);
        free(baseTree);
    }
}

void UCTEngine3::resetGame() {
    resetTree = true;
}

void UCTEngine3::playMove(int coord) {
    if (!resetTree && !isPass(coord) && baseTree && baseTree->sons) {
        nextTurnTree(coord);
    }
    else resetTree = true;
    //if (baseTree) printTree(0, baseTree, 0);
}

void UCTEngine3::nextTurnTree(int coord) {
    char* x = (char*)malloc(sizeof(*baseTree));
    uctNode * tmp = (uctNode*)x;
    tmp->tot = baseTree->sons[coord].tot;
    tmp->win = baseTree->sons[coord].win;
    tmp->value = baseTree->sons[coord].value;
    tmp->vtot = baseTree->sons[coord].vtot;
    tmp->vwin = baseTree->sons[coord].vwin;
    tmp->vvalue = baseTree->sons[coord].vvalue;
    tmp->sons = baseTree->sons[coord].sons;
    for (int i = 0; i < size2; ++i) {
        if (i != coord) eraseSons(&baseTree->sons[i]);
    }
    free(baseTree->sons);
    free(baseTree);
    baseTree = tmp;
    baseTree->dad = 0;
    if (baseTree->sons) {
        for (int i = 0; i < size2; ++i) baseTree->sons[i].dad = baseTree;
    }
    //printTree(0, baseTree, 0);
    
}

void UCTEngine3::printTree(int mov, uctNode * tree, int spaces) {
    if (spaces > 6) return;
    if (tree->tot > 0) {
        for (int i = 0; i < spaces; ++i) fprintf(stderr, " ");
        float res = 0;
        if (tree->dad) {
            res = tree->value + P_3 * sqrt(log(tree->dad->tot)/tree->tot);
            float res = (tree->value*tree->tot + tree->vvalue*tree->vtot)/
                        (tree->tot+tree->vtot) + P_3 *
                sqrt(log(tree->dad->tot+tree->dad->vtot)/(tree->tot+tree->vtot));
        }
        fprintf(stderr, "%i,%i - %i %i %f %f %i %i\n", mov/size, mov%size,
                tree->win, tree->tot, tree->value, res, tree->vwin, tree->vtot);
    }
    if (tree->sons) {
        for (int i = size; i < size2 - size; ++i) {
            printTree(i, &tree->sons[i], spaces+2);
        }
    }
}

inline uctNode* UCTEngine3::getBestMove(uctNode * tree, GoBoard3& b,
                                           char& col) {
    GoBoard3 orig = GoBoard3(b);
    while(tree->sons) {
        int mov = getBestSon(tree, b);
        b.playMove(mov, col + 1);
        if (b.superko) {
            //fprintf(stderr, "superko %i,%i\n", mov/size, mov%size);
            tree->sons[mov].tot = -1;
            b.resetStateBoard(orig);
        }
        else {
            col = !col;
            if (isPass(mov)) return tree;
            tree = &tree->sons[mov];
        }
    }
    return tree;    
}

inline void UCTEngine3::expandNode(uctNode * tree, GoBoard3& board, 
                                     char color) {
    int memsize = size2 * sizeof(*(tree->sons));
    char* x = (char*)malloc(memsize);
    tree->sons = (uctNode*)x;
    memset(tree->sons, 0, memsize);
    for (int i = 0; i < size2; ++i) tree->sons[i].dad = tree;
    //int totwin = 0;
    //int tottot = 0;
    for (int i = 0; i < board.flen; ++i) {
        int c = board.f[i];
        int wins = 0;
        int loses = 0;
        int bonus = board.getBonus(c, color + 1, wins, loses);
        if (bonus == FORBIDBONUS) tree->sons[c].tot = -1;
        else if (wins || loses) {
            tree->sons[c].vwin = wins;
            tree->sons[c].vtot = loses;
            tree->sons[c].vvalue = wins / (wins + loses);
            //tottot += bonus;
            //totwin += bonus;
        }
    }
    //should we expand bonus to parents? if we don't, is it ok to consider
    //the virtual simulations in the log?
    /*if (tottot > 0) {
        while(tree) {
            totwin = tottot-totwin;
            tree->tot += tottot;
            tree->win += totwin;
            tree->value = double(tree->win) / tree->tot;
            tree = tree->dad;
        }
    }*/
}

void UCTEngine3::eraseSons(uctNode * tree) {
    if (tree->sons) {
        for (int i = 0; i < size2; ++i) {
            eraseSons(&tree->sons[i]);
        }
        free(tree->sons);
    }
}

inline int UCTEngine3::getBestSon(uctNode * tree, GoBoard3& board) {
    double best = 0;
    int mov = -1;
    double logtot = log(tree->tot + tree->vtot);
    for (int i = 0; i < board.flen; ++i) {
        int ii = board.f[i];
        int sontot = tree->sons[ii].tot + tree->sons[ii].vtot;
        float res = -1;
        if (tree->sons[ii].tot == 0 && FPU_3 > best) {
            best = FPU_3;
            mov = ii;
        }
        else if (tree->sons[ii].tot > 0) {
            float res = (tree->sons[ii].value*tree->sons[ii].tot +
                         tree->sons[ii].vvalue*tree->sons[ii].vtot)/sontot;
                         + P_3 * sqrt(logtot/sontot);
            if (res > best) {
                best = res;
                mov = ii;
            }
        }
    }
    return mov;
}

inline int UCTEngine3::getBestMove(uctNode * tree) {
    double best = 0;
    int mov = -1;
    for (int i = size; i < size2-size; ++i) {
        if (tree->sons[i].tot > best) {
            best = tree->sons[i].tot;
            mov = i;
        }
    }
    // consider pass 
    return mov;
}


inline void UCTEngine3::updateSon(uctNode * tree, char col, char ind,
                                  float score) {
    //fprintf(stderr, "score: %f\n", score);
    char addWin = !col;
    if (score > 0) addWin = col;
    tree->tot++;
    tree->win += addWin;
    if (tree->tot > 1) {
        tree->value += (addWin - tree->value) / tree->tot;
    }
    else tree->value = addWin;
    addWin = !addWin;
    tree = tree->dad;
    //fprintf(stderr, "getBestMove\n");
    while(tree) {
        //fprintf(stderr, "dad1\n");
        tree->tot++;
        tree->win += addWin;
        tree->value += (addWin - tree->value) / tree->tot;
        tree = tree->dad;
        addWin = !addWin;
    }
}

GoMove UCTEngine3::nextMove(GoBoard3& board, char color) {
    //board.testPatterns(color);
    //return GoMove(-1, -1, color, true);
    size = board.size;
    size2 = board.size2;
    color = (color == WHITE);
    if (resetTree || !baseTree) {
        if (baseTree) {
            eraseSons(baseTree);
            free(baseTree);
        }
        if (resetTree) resetTree = false;
        char* x = (char*)malloc(sizeof(*baseTree));
        baseTree = (uctNode*)x;
        memset(baseTree, 0, sizeof(*baseTree));
        expandNode(baseTree, board, color);
    }
    if (!baseTree->sons) {
        expandNode(baseTree, board, color);
    }
    /*for (int i = size; i < size2 - size; ++i) {
        float res = 0;
        res = baseTree->sons[i].value +
              P_3 * sqrt(log(baseTree->tot)/baseTree->sons[i].tot);
        if (baseTree->sons[i].tot > 0) {
            fprintf(stderr, "%i,%i - %i %i %f %f\n", i/size, i%size,
                                  baseTree->sons[i].win, baseTree->sons[i].tot,
                                  baseTree->sons[i].value, res);
        }
    
    }
    fprintf(stderr, "\n\n");*/
    int it = 30000;
    int cont = 0;
    int countsuperko = 0;
    GoBoard3 b2 = GoBoard3(board);
    if (isPass(board.lastMove)) { // this is a very ugly fix
        int win = 0;
        int sim = it;
        while(it) {
            b2.playMove(PASS, color + 1);
            float score = b2.playRandomGame(!color, 0);
            if (!b2.superko) {
                if (score > 0) win += color;
                else win += !color;
                it--;
            }
            b2.resetStateBoard(board);
        }
        if (double(win) / sim > 0.7) return GoMove(-1, -1, color, true);
    }
    it = 30000;
    while(it) {
        cont++;
        //fprintf(stderr, "cont\n");
        char col = color;
        //fprintf(stderr, "getbestMove\n");
        uctNode * bestMove = getBestMove(baseTree, b2, col);
        if (bestMove->tot > 6 && !bestMove->sons) {
            //fprintf(stderr, "expandMove\n");
            expandNode(bestMove, b2, col);
        }
        
        if (isPass(b2.lastMove)) {
            float score = b2.playRandomGame(!col, 0);
            //fprintf(stderr, "updateSonPass\n");
            updateSon(bestMove, col, PASS, score);
            --it;
        }
        else {
            //fprintf(stderr, "playHeuristic\n");
            char ind = b2.playHeuristicMove(col + 1);
            if ((isPass(ind) || (ind > size && b2.g[ind])) && !b2.superko) {
                //fprintf(stderr, "randomGame\n");
                float score = b2.playRandomGame(!col, 0);
                if (!b2.superko) {
                    //fprintf(stderr, "updateSon\n");
                    updateSon(bestMove, !col, ind, score);
                    //fprintf(stderr, "turns: %i\n", b2.turns);
                    //b2.printBoard();
                    it--;
                }
            }
            if (b2.superko) {
                //fprintf(stderr, "SUPERKOOOOOO!!!\n");
                countsuperko++;
            }
        }
        //fprintf(stderr, "resetBoard\n");
        b2.resetStateBoard(board);
    }
    //fprintf(stderr, "cont:%i\n", cont);
    //fprintf(stderr, "contsuperko:%i\n", countsuperko);
    //int mov = getBestSon(baseTree, board);
    //fprintf(stderr, "getBestMoveFin\n");
    int mov = getBestMove(baseTree);
    if (mov != PASS && baseTree->sons[mov].value < 0.2) mov = RESIGN;
    //board.printBoard();
    /*fprintf(stderr, "tot %i\n", baseTree->tot);
    for (int i = size; i < size2 - size; ++i) {
        float res = 0;
        res = baseTree->sons[i].value +
              P_3 * sqrt(log(baseTree->tot)/baseTree->sons[i].tot);
        if (baseTree->sons[i].tot > 0) {
            fprintf(stderr, "%i,%i - %i %i %f %f\n", i/size, i%size,
                                  baseTree->sons[i].win, baseTree->sons[i].tot,
                                  baseTree->sons[i].value, res);
        }
    
    }
    fprintf(stderr, "\n\n");*/
    //printTree(0, baseTree, 0);
    if (!isPass(mov) && !isResign(mov)) {
        nextTurnTree(mov);
        return GoMove(mov/size - 1, mov%size - 1, color, true);
    }
    else if (isPass(mov)) {
        resetTree = true;
        return GoMove(-1, -1, color, true);
    }
    else {
        resetTree = true;
        return GoMove(-1, -1, color, false);
    }
}

