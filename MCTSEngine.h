/* 
 * File:   MCTSEngine.h
 * Author: Red
 *
 * Created on 20 de marzo de 2013, 20:11
 */

#ifndef MCTSENGINE_H
#define	MCTSENGINE_H

#include "GoEngine.h"
#include "MCTSTree.h"

typedef std::vector<int> VecI;

class MCTSEngine : public GoEngine {
    
public:
    MCTSEngine();
    MCTSEngine(const MCTSEngine& orig);
    virtual ~MCTSEngine();
    virtual GoMove nextMove(GoBoard2& board, char color);
    
private:

};

#endif	/* MCTSENGINE_H */

