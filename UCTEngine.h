/* 
 * File:   UCTEngine.h
 * Author: Red
 *
 * Created on 24 de marzo de 2013, 23:21
 */

#ifndef UCTENGINE_H
#define	UCTENGINE_H

#include "GoEngine.h"
#include "UCTTree.h"
#include "GoBoard2.h"

typedef std::vector<int> VecI;

class UCTEngine : public GoEngine {
    
public:
    UCTEngine();
    UCTEngine(const UCTEngine& orig);
    virtual ~UCTEngine();
    virtual GoMove nextMove(GoBoard2& board, char color);
    
private:

};

#endif	/* UCTENGINE_H */

