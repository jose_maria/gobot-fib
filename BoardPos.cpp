/* 
 * File:   BoardPos.cpp
 * Author: Red
 * 
 * Created on 19 de octubre de 2012, 23:50
 */

#include <iostream>
#include "BoardPos.h"

BoardPos::BoardPos() {
    //type = 9;
}

BoardPos::BoardPos(const BoardPos& orig) {
}

BoardPos::~BoardPos() {
}

bool BoardPos::isBlack2() {
    return type == 0;
}

bool BoardPos::isWhite2() {
    return type == 1;
}

bool BoardPos::koMark() {
    return type == 8;
}

bool BoardPos::isEmpty2() {
    return type == 9;
}

bool BoardPos::hasGroup() {
    return group != 0;
}
