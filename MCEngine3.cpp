/* 
 * File:   MCEngine3.cpp
 * Author: Red
 * 
 * Created on 15 de abril de 2013, 21:15
 */


#include "MCEngine3.h"

MCEngine3::MCEngine3() {
}

MCEngine3::MCEngine3(const MCEngine3& orig) {
}

MCEngine3::~MCEngine3() {
}

GoMove MCEngine3::nextMove(GoBoard3& board, char color) {
    mcMove3 moves[board.size2];
    memset(moves, 0 , sizeof(moves));
    color = (color == WHITE);
    int it = 30000;
    GoBoard3 b2 = GoBoard3(board);
    while(it--) {
        char col = color;
        char ind = b2.playHeuristicMove(col + 1);
        if (b2.g[ind]) {
            moves[ind].tot++;
            float score = b2.playRandomGame(!col, 0);
            if (score > 0) moves[ind].win += color;
            else moves[ind].win += !color;
        }
        b2.resetStateBoard(board);
    }
    double best = -1;
    char mov = -1;
    for (int i = 0; i < board.size2; ++i) {
        int i2 = i/board.size;
        int j2 = i%board.size;
        if (board.turns > 3 || (2 < i2 && i2 < 8 && 2 < j2 && j2 < 8)) {
            if (moves[i].tot > 0) {
                double temp = (double)moves[i].win/(double)moves[i].tot;
                if (temp > best) {
                    best = temp;
                    mov = i;
                }
            }
        }
    }
    if (!isPass(mov) && !isResign(mov)) {
        return GoMove(mov/board.size - 1, mov%board.size - 1, color, true);
    }
    else if (isPass(mov)) {
        return GoMove(-1, -1, color, true);
    }
    else return GoMove(-1, -1, color, false);
}
