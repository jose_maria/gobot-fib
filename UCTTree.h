/* 
 * File:   UCTTree.h
 * Author: Red
 *
 * Created on 25 de marzo de 2013, 21:04
 */

#ifndef UCTTREE_H
#define	UCTTREE_H

#include <vector>
#include "GoBoard2.h"
#include <math.h>
#define P 0.3
#define FPU 10000

typedef std::vector<int> VecI;

class UCTTree {
    
public:

    UCTTree();
    UCTTree(const UCTTree& orig);
    UCTTree(UCTTree* dad2, GoBoard2 * s, int i, int j, int col2,
             double v, int w, int t);
    virtual ~UCTTree();
    int getBest();
    void updateNode(int);
    void expandNode();
    void printTree(int sp);
    
    UCTTree * dad;
    std::vector<UCTTree*> sons;
    double value;
    int i;
    int j;
    int col;
    int win;
    int tot;
    GoBoard2 * state;
    
private:

};

#endif	/* UCTTREE_H */

