/* 
 * File:   MCEngine3.h
 * Author: Red
 *
 * Created on 15 de abril de 2013, 21:15
 */

#ifndef MCENGINE3_H
#define	MCENGINE3_H

#include "GoEngine3.h"
#include <algorithm>

struct mcMove3 {
    int win;
    int tot;
};

class MCEngine3 : public GoEngine3 {
    
public:
    MCEngine3();
    MCEngine3(const MCEngine3& orig);
    virtual ~MCEngine3();
    virtual GoMove nextMove(GoBoard3& board, char color);
    
private:
    
};

#endif	/* MCENGINE3_H */

