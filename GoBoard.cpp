/* 
 * File:   GoBoard.cpp
 * Author: Red
 * 
 * Created on 19 de octubre de 2012, 23:27
 */

#include <stdlib.h>
#include "GoBoard.h"

GoBoard::GoBoard() {
}

GoBoard::GoBoard(char x) {
    SIZE = x;
    clearBoard();
}

GoBoard::GoBoard(const GoBoard& orig) {
    SIZE = orig.SIZE;
    capWhite = orig.capWhite;
    capBlack = orig.capBlack;
    scoreWhite = orig.scoreWhite;
    scoreBlack = orig.scoreWhite;
    komi = orig.komi;
    zob = orig.zob;
    hashAct = hashAct;
    hashes = orig.hashes;
    movesBlack = orig.movesBlack;
    movesWhite = orig.movesWhite;
    board = Mat(SIZE, Vec(SIZE));
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            board[i][j].type = orig.board[i][j].type;
            board[i][j].i = orig.board[i][j].i;
            board[i][j].j = orig.board[i][j].j;
        }
    }
    for (ItG it = orig.bGroups.begin(); it != orig.bGroups.end(); ++it) {
        StoneGroup* newg = new StoneGroup();
        StoneGroup* gr = *it;
        newg->color = gr->color;
        for (It it2 = (gr->stones).begin(); it2 != (gr->stones).end(); ++it2) {    
            BoardPos* ps = *it2;
            newg->addStone(&board[ps->i][ps->j]);
            board[ps->i][ps->j].group = newg;
        };
        for (It it2 = (gr->liberties).begin(); it2 != (gr->liberties).end(); ++it2) {    
            BoardPos* ps = *it2;
            (newg->liberties).insert(&board[ps->i][ps->j]);
        };
        bGroups.insert(newg);
    }
    for (ItG it = orig.wGroups.begin(); it != orig.wGroups.end(); ++it) {
        StoneGroup* newg = new StoneGroup();
        StoneGroup* gr = *it;
        newg->color = gr->color;
        for (It it2 = (gr->stones).begin(); it2 != (gr->stones).end(); ++it2) {    
            BoardPos* ps = *it2;
            newg->addStone(&board[ps->i][ps->j]);
            board[ps->i][ps->j].group = newg;
        };
        for (It it2 = (gr->liberties).begin(); it2 != (gr->liberties).end(); ++it2) {    
            BoardPos* ps = *it2;
            (newg->liberties).insert(&board[ps->i][ps->j]);
        };
        wGroups.insert(newg);
    }
    for (It it = orig.setKo.begin(); it != orig.setKo.end(); ++it) {    
        BoardPos* ps = *it;
        setKo.insert(&board[ps->i][ps->j]);
    };
}

GoBoard::~GoBoard() {
    for (ItG it = bGroups.begin(); it != bGroups.end(); ++it) {
        StoneGroup* gr = *it;
        delete gr;
    }
    for (ItG it = wGroups.begin(); it != wGroups.end(); ++it) {
        StoneGroup* gr = *it;
        delete gr;
    }
}

void GoBoard::initZob() {
    zob = VZob(SIZE, MChar(SIZE, VChar(3, 2)));
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            zob[i][j][0] = rand();
            zob[i][j][1] = rand();
            zob[i][j][2] = rand();
        }
    }
}

int GoBoard::hashZob() {
    int key = 0;
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            int stat = board[i][j].type;
            if (stat > 2) stat = 2;
            key ^= zob[i][j][stat];
        }
    }
    return key;
}

void GoBoard::clearBoard() {
    initZob();
    hashes.clear();
    movesBlack.clear();
    movesWhite.clear();
    bGroups.clear();
    wGroups.clear();
    setKo.clear();
    komi = 6.5;
    capWhite = 0;
    capBlack = 0;
    scoreWhite = 0;
    scoreBlack = 0;
    board = Mat(SIZE, Vec(SIZE));
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            board[i][j].group = 0;
            board[i][j].type = 9;
            board[i][j].i = i;
            board[i][j].j = j;
            movesBlack.insert(i*SIZE + j);
            movesWhite.insert(i*SIZE + j);
        }
    }
    hashAct = hashZob();
    hashes.insert(hashAct);
}

void GoBoard::setKomi(double k) {
    komi = k;
}

void GoBoard::fusionGroups(StoneGroup * g1, StoneGroup * g2) {
    if ((*g1).color == 0) {
        bGroups.erase(g1);
        bGroups.erase(g2);
    }
    else {
        wGroups.erase(g1);
        wGroups.erase(g2);
    }
    if (DEBUG) std::cerr << "fusionGroups" << ' ' << g1 << ' ' << g2 << std::endl;
    for (It it = (g2->stones).begin(); it != (g2->stones).end(); ++it) {
        if (DEBUG) std::cerr << (int)(**it).i << ',' << (int)(**it).j << ' ';
        (**it).group = g1;
        g1->addStone(*it);
    }
    g1->liberties.insert((g2->liberties).begin(), (g2->liberties).end());
    if (DEBUG) std::cerr << std::endl;
    delete g2;
    if ((*g1).color == 0) bGroups.insert(g1);
    else wGroups.insert(g1);
}

StoneGroup* GoBoard::createGroup(char i, char j, char color) {
    StoneGroup* g = new StoneGroup(&board[i][j], color);
    if (i > 0 && board[i-1][j].type >= 2) {
        (g->liberties).insert(&board[i-1][j]);
    }
    if (j > 0 && board[i][j-1].type >= 2) {
        (g->liberties).insert(&board[i][j-1]);
    }
    if (i+1 < SIZE && board[i+1][j].type >= 2) {
        (g->liberties).insert(&board[i+1][j]);
    }
    if (j+1 < SIZE && board[i][j+1].type >= 2) {
        (g->liberties).insert(&board[i][j+1]);
    }
    if (color == 0) bGroups.insert(g);
    else wGroups.insert(g);
    return g;
}

void GoBoard::addToGroups(char i, char j, char color) {
    if (DEBUG) std::cerr << "CreateGroup" << std::endl;
    StoneGroup* group = createGroup(i, j, color);
    board[i][j].group = group;
    if (i > 0) {
        if (board[i-1][j].type == color) {
            if (board[i-1][j].group != group) {
                fusionGroups(board[i][j].group, board[i-1][j].group);
                board[i-1][j].group = board[i][j].group;
            }
        }
        if (board[i-1][j].type <= 2) {
            (board[i-1][j].group->liberties).erase(&board[i][j]);
        }
    }
    if (j > 0) {
        if (board[i][j-1].type == color) {
            if (board[i][j-1].group != group) {
                fusionGroups(board[i][j].group, board[i][j-1].group);
                board[i][j-1].group = board[i][j].group;
            }
        }
        if (board[i][j-1].type <= 2) {
            (board[i][j-1].group->liberties).erase(&board[i][j]);
        }
    }
    if (i+1 < SIZE) {
        if (board[i+1][j].type == color) {
            if (board[i+1][j].group != group) {
                fusionGroups(board[i][j].group, board[i+1][j].group);
                board[i+1][j].group = board[i][j].group;
            }
        }
        if (board[i+1][j].type <= 2) {
            (board[i+1][j].group->liberties).erase(&board[i][j]);
        }
    }
    if (j+1 < SIZE) {
        if (board[i][j+1].type == color) {
            if (board[i][j+1].group != group) {
                fusionGroups(board[i][j].group, board[i][j+1].group);
                board[i][j+1].group = board[i][j].group;
            }
        }
        if (board[i][j+1].type <= 2) {
            (board[i][j+1].group->liberties).erase(&board[i][j]);
        }
    }
}

bool GoBoard::isSuicide(char i, char j, char color) {
    if (DEBUG) std::cerr << "isSuicide " << (int)i << ',' << (int)j << ' '
                         << int(color) << std::endl;
    char color2;
    if (color == 0) color2 = 1;
    else color2 = 0;
    
    if ((i-1 >= 0 && board[i-1][j].type == color2 && 
        (board[i-1][j].group->liberties).size() == 1) ||
        (j-1 >= 0 && board[i][j-1].type == color2 && 
        (board[i][j-1].group->liberties).size() == 1) ||
        (i+1 < SIZE && board[i+1][j].type == color2 && 
        (board[i+1][j].group->liberties).size() == 1) ||
        (j+1 < SIZE && board[i][j+1].type == color2 && 
        (board[i][j+1].group->liberties).size() == 1)) {
                if (DEBUG) std::cerr << "2if" << std::endl;
                return false;
    }
    
    if ((i-1 < 0 || board[i-1][j].type == color2) &&
        (j-1 < 0 || board[i][j-1].type == color2) && 
        (i+1 >= SIZE || board[i+1][j].type == color2) && 
        (j+1 >= SIZE || board[i][j+1].type == color2)) { 
                if (DEBUG) std::cerr << "1if" << std::endl;
                return true;
    }
    
    // check if it suicides a group
    // if its next to a group with 1 liberty and it cant connect with
    // a group with more than 1 liberty, its a suicide move
    if ((i-1 >= 0 && board[i-1][j].type == color && 
        (board[i-1][j].group->liberties).size() == 1) &&
        !((j-1 >= 0 && board[i][j-1].type > 2) ||
         (i+1 < SIZE && board[i+1][j].type > 2) ||
         (j+1 < SIZE && board[i][j+1].type > 2) ||
         (j-1 >= 0 && board[i][j-1].type == color
          && (board[i][j-1].group->liberties).size() > 1) ||
         (i+1 < SIZE && board[i+1][j].type == color
          && (board[i+1][j].group->liberties).size() > 1) ||
         (j+1 < SIZE && board[i][j+1].type == color
          && (board[i][j+1].group->liberties).size() > 1))) {
                return true;
    }
    if  ((j-1 >= 0 && board[i][j-1].type == color && 
        (board[i][j-1].group->liberties).size() == 1) &&
        !((i-1 >= 0 && board[i-1][j].type > 2) ||
         (i+1 < SIZE && board[i+1][j].type > 2) ||
         (j+1 < SIZE && board[i][j+1].type > 2) ||
         (i-1 >= 0 && board[i-1][j].type == color
          && (board[i-1][j].group->liberties).size() > 1) ||
         (i+1 < SIZE && board[i+1][j].type == color
          && (board[i+1][j].group->liberties).size() > 1) ||
         (j+1 < SIZE && board[i][j+1].type == color
          && (board[i][j+1].group->liberties).size() > 1))) {
                return true;
    }
    
    if  ((i+1 < SIZE && board[i+1][j].type == color && 
        (board[i+1][j].group->liberties).size() == 1) && 
        !((i-1 >= 0 && board[i-1][j].type > 2) ||
         (j-1 >= 0 && board[i][j-1].type > 2) ||
         (j+1 < SIZE && board[i][j+1].type > 2) ||
         (i-1 >= 0 && board[i-1][j].type == color
          && (board[i-1][j].group->liberties).size() > 1) ||
         (j-1 >= 0 && board[i][j-1].type == color
          && (board[i][j-1].group->liberties).size() > 1) ||
         (j+1 < SIZE && board[i][j+1].type == color
          && (board[i][j+1].group->liberties).size() > 1))) {
                return true;
    }
    if  ((j+1 < SIZE && board[i][j+1].type == color && 
        (board[i][j+1].group->liberties).size() == 1) &&
        !((i-1 >= 0 && board[i-1][j].type > 2) ||
         (j-1 >= 0 && board[i][j-1].type > 2) ||
         (i+1 < SIZE && board[i+1][j].type > 2) ||
         (i-1 >= 0 && board[i-1][j].type == color
          && (board[i-1][j].group->liberties).size() > 1) ||
         (j-1 >= 0 && board[i][j-1].type == color
          && (board[i][j-1].group->liberties).size() > 1) ||
         (i+1 < SIZE && board[i+1][j].type == color
          && (board[i+1][j].group->liberties).size() > 1))) {
                return true;
    }
    return false;
}

void GoBoard::killStone(BoardPos * pos) {
    if (DEBUG) std::cerr << "killStone" << std::endl;
    char color = pos->type;
    if (color == 0) color = 1;
    else color = 0;
    int i = pos->i;
    int j = pos->j;
    if (i > 0 && board[i-1][j].type == color) {
        if (board[i-1][j].group->liberties.size() == 1) {
            BoardPos * p = *(board[i-1][j].group->liberties.begin());
            if (color == 0 && !isSuicide(p->i, p->j, 0)) {
                movesBlack.insert(p->i*SIZE + p->j);
            }
            else if (!isSuicide(p->i, p->j, 1)) {
                movesWhite.insert(p->i*SIZE + p->j);
            }
        }
        (board[i-1][j].group->liberties).insert(&board[i][j]);
    }
    if (j > 0 && board[i][j-1].type == color) {
        if (board[i][j-1].group->liberties.size() == 1) {
            BoardPos * p = *(board[i][j-1].group->liberties.begin());
            if (color == 0 && !isSuicide(p->i, p->j, 0)) {
                movesBlack.insert(p->i*SIZE + p->j);
            }
            else if (!isSuicide(p->i, p->j, 1)) {
                movesWhite.insert(p->i*SIZE + p->j);
            }
        }
        (board[i][j-1].group->liberties).insert(&board[i][j]);
    }
    if (i+1 < SIZE && board[i+1][j].type == color) {
        if (board[i+1][j].group->liberties.size() == 1) {
            BoardPos * p = *(board[i+1][j].group->liberties.begin());
            if (color == 0 && !isSuicide(p->i, p->j, 0)) {
                movesBlack.insert(p->i*SIZE + p->j);
            }
            else if (!isSuicide(p->i, p->j, 1)) {
                movesWhite.insert(p->i*SIZE + p->j);
            }
        }
        (board[i+1][j].group->liberties).insert(&board[i][j]);
    }
    if (j+1 < SIZE && board[i][j+1].type == color) {
        if (board[i][j+1].group->liberties.size() == 1) {
            BoardPos * p = *(board[i][j+1].group->liberties.begin());
            if (color == 0 && !isSuicide(p->i, p->j, 0)) {
                movesBlack.insert(p->i*SIZE + p->j);
            }
            else if (!isSuicide(p->i, p->j, 1)) {
                movesWhite.insert(p->i*SIZE + p->j);
            }
        }
        (board[i][j+1].group->liberties).insert(&board[i][j]);
    }
    hashAct ^= zob[i][j][board[i][j].type];
    hashAct ^= zob[i][j][2];
    movesBlack.insert(i*SIZE + j);
    movesWhite.insert(i*SIZE + j);
    board[i][j].type = 9;
    board[i][j].group = 0;
}

bool GoBoard::killGroup(StoneGroup * g) {
    if (DEBUG) std::cerr << "killGroup" << std::endl;
    int tam = (g->stones).size();
    bool ko = false;
    if (tam == 1) {
        It it = (g->stones).begin();
        killStone(*it);
        ko = true;
    }
    else {
        for (It it = (g->stones).begin(); it != (g->stones).end(); ++it) {
            killStone(*it);
        }
    }
    if (g->color == 0) {
        capWhite += tam;
        bGroups.erase(g);
    }
    else {
        capBlack += tam;
        wGroups.erase(g);
    }
    delete g;
    return ko;
}

bool GoBoard::isAtari1(char i, char j, char color) {
    if (i < 0 && board[i-1][j].type == color) {
        (board[i-1][j].group->liberties).insert(&board[i][j]);
    }
    if (j > 0 && board[i][j-1].type == color) {
        (board[i][j-1].group->liberties).insert(&board[i][j]);
    }
    if (i+1 < SIZE && board[i+1][j].type == color) {
        (board[i+1][j].group->liberties).insert(&board[i][j]);
    }
    if (j+1 < SIZE && board[i][j+1].type == color) {
        (board[i][j+1].group->liberties).insert(&board[i][j]);
    }
}

void GoBoard::killAdjacent(char i, char j, char color) {
    if (DEBUG) std::cerr << "killAdjacent" << std::endl;
    int color2 = 0;
    if (color == 0) color2 = 1;
    if (i > 0 && board[i-1][j].type == color2 && 
        (board[i-1][j].group->liberties).size() == 1) {
        if (killGroup(board[i-1][j].group)) {
            if ((j <= 0 || board[i][j-1].type == color2) && 
               (i+1 >= SIZE || board[i+1][j].type == color2) && 
               (j+1 >= SIZE || board[i][j+1].type == color2)) {
                setKo.insert(&board[i-1][j]);
                movesBlack.erase((i-1)*SIZE + j);
                movesWhite.erase((i-1)*SIZE + j);
                board[i-1][j].type = 8;
            }
        }
    }
    if (j > 0 && board[i][j-1].type == color2 && 
        (board[i][j-1].group->liberties).size() == 1) {
        if (killGroup(board[i][j-1].group)) {
            if ((i <= 0 || board[i-1][j].type == color2) &&
               (i+1 >= SIZE || board[i+1][j].type == color2) && 
               (j+1 >= SIZE || board[i][j+1].type == color2)) {
                setKo.insert(&board[i][j-1]);
                movesBlack.erase(i*SIZE + j-1);
                movesWhite.erase(i*SIZE + j-1);
                board[i][j-1].type = 8;
            }
        }
    }
    if (i+1 < SIZE && board[i+1][j].type == color2 && 
        (board[i+1][j].group->liberties).size() == 1) {
        if (killGroup(board[i+1][j].group)) {
            if ((i <= 0 || board[i-1][j].type == color2) &&
               (j <= 0 || board[i][j-1].type == color2) && 
               (j+1 >= SIZE || board[i][j+1].type == color2)) {
                setKo.insert(&board[i+1][j]);
                movesBlack.erase((i+1)*SIZE + j);
                movesWhite.erase((i+1)*SIZE + j);
                board[i+1][j].type = 8;
            }
        }
    }
    if (j+1 < SIZE && board[i][j+1].type == color2 && 
        (board[i][j+1].group->liberties).size() == 1) {
        if (killGroup(board[i][j+1].group)) {
            if ((i <= 0 || board[i-1][j].type == color2) &&
               (j <= 0 || board[i][j-1].type == color2) && 
               (i+1 >= SIZE || board[i+1][j].type == color2)) {
                movesBlack.erase(i*SIZE + j+1);
                movesWhite.erase(i*SIZE + j+1);
                setKo.insert(&board[i][j+1]);
                board[i][j+1].type = 8;
            }
        }
    }
}

void GoBoard::updateMovesBlack(char i, char j) {
    if (board[i][j].group->liberties.size() == 1) {
        BoardPos * p = *board[i][j].group->liberties.begin();
        movesWhite.insert((p->i)*SIZE + p->j);
    }
    if (i > 0) {
        if (board[i-1][j].type > 2) {
            if (isEye(i-1, j, 0)) movesBlack.erase((i-1)*SIZE + j);
            if (isSuicide(i-1, j, 1)) movesWhite.erase((i-1)*SIZE + j);
            if (isSuicide(i-1, j, 0)) movesBlack.erase((i-1)*SIZE + j);
        }
        else if (board[i-1][j].type == 1 &&
                (board[i-1][j].group->liberties.size()) == 1) {
            BoardPos * lib = *((board[i-1][j].group->liberties).begin());
            movesWhite.erase((lib->i)*SIZE + lib->j);
            movesBlack.insert((lib->i)*SIZE + lib->j);
        }
    }
    if (j > 0) {
        if (board[i][j-1].type > 2) {
            if (isEye(i, j-1, 0)) movesBlack.erase(i*SIZE + j-1);
            if (isSuicide(i, j-1, 1)) movesWhite.erase(i*SIZE + j-1);
            if (isSuicide(i, j-1, 0)) movesBlack.erase(i*SIZE + j-1);
        }
        else if (board[i][j-1].type == 1 &&
                 (board[i][j-1].group->liberties.size()) == 1) {
            BoardPos * lib = *((board[i][j-1].group->liberties).begin());
            movesWhite.erase((lib->i)*SIZE + lib->j);
            movesBlack.insert((lib->i)*SIZE + lib->j);
        }
    }
    if (i+1 < SIZE) {
        if (board[i+1][j].type > 2) {
            if (isEye(i+1, j, 0)) movesBlack.erase((i+1)*SIZE + j);
            if (isSuicide(i+1, j, 1)) movesWhite.erase((i+1)*SIZE + j);
            if (isSuicide(i+1, j, 0)) movesBlack.erase((i+1)*SIZE + j);
        }
        else if (board[i+1][j].type == 1 &&
                 (board[i+1][j].group->liberties.size()) == 1) {
            BoardPos * lib = *((board[i+1][j].group->liberties).begin());
            movesWhite.erase((lib->i)*SIZE + lib->j);
            movesBlack.insert((lib->i)*SIZE + lib->j);
        }
    }
    if (j+1 < SIZE) {
        if (board[i][j+1].type > 2) {
            if (isEye(i, j+1, 0)) movesBlack.erase(i*SIZE + j+1);
            if (isSuicide(i, j+1, 1)) movesWhite.erase(i*SIZE + j+1);
            if (isSuicide(i, j+1, 0)) movesBlack.erase(i*SIZE + j+1);
        }
        else if (board[i][j+1].type == 1 &&
                 (board[i][j+1].group->liberties.size()) == 1) {
            BoardPos * lib = *((board[i][j+1].group->liberties).begin());
            movesWhite.erase((lib->i)*SIZE + lib->j);
            movesBlack.insert((lib->i)*SIZE + lib->j);
        }
    }
    if (i > 0 && j > 0 && board[i-1][j-1].type > 2 && 
        isEye(i-1, j-1, 0)) {
        movesBlack.erase((i-1)*SIZE + j-1);
    }
    if (i > 0 && j+1 < SIZE && board[i-1][j+1].type > 2 && 
        isEye(i-1, j+1, 0)) {
        movesBlack.erase((i-1)*SIZE + j+1);
    }
    if (i+1 < SIZE && j > 0 && board[i+1][j-1].type > 2 && 
        isEye(i+1, j-1, 0)) {
        movesBlack.erase((i+1)*SIZE + j-1);
    }
    if (i+1 < SIZE && j+1 < SIZE && board[i+1][j+1].type > 2 && 
        isEye(i+1, j+1, 0)) {
        movesBlack.erase((i+1)*SIZE + j+1);
    }
}

void GoBoard::updateMovesWhite(char i, char j) {
    if (board[i][j].group->liberties.size() == 1) {
        BoardPos * p = *board[i][j].group->liberties.begin();
        movesBlack.insert((p->i)*SIZE + p->j);
    }
    if (i > 0) {
        if (board[i-1][j].type > 2) {
            if (isEye(i-1, j, 1)) movesWhite.erase((i-1)*SIZE + j);
            if (isSuicide(i-1, j, 0)) movesBlack.erase((i-1)*SIZE + j);
            if (isSuicide(i-1, j, 1)) movesWhite.erase((i-1)*SIZE + j);
        }
        else if (board[i-1][j].type == 0 &&
                 (board[i-1][j].group->liberties.size()) == 1) {
            BoardPos * lib = *((board[i-1][j].group->liberties).begin());
            movesBlack.erase((lib->i)*SIZE + lib->j);
            movesWhite.insert((lib->i)*SIZE + lib->j);
        }
    }
    if (j > 0) {
        if (board[i][j-1].type > 2) {
            if (isEye(i, j-1, 1)) movesWhite.erase(i*SIZE + j-1);
            if (isSuicide(i, j-1, 0)) movesBlack.erase(i*SIZE + j-1);
            if (isSuicide(i, j-1, 1)) movesWhite.erase(i*SIZE + j-1);
        }
        else if (board[i][j-1].type == 0 &&
                 (board[i][j-1].group->liberties.size()) == 1) {
            BoardPos * lib = *((board[i][j-1].group->liberties).begin());
            movesBlack.erase((lib->i)*SIZE + lib->j);
            movesWhite.insert((lib->i)*SIZE + lib->j);
        }
    }
    if (i+1 < SIZE) {
        if (board[i+1][j].type > 2) {
            if (isEye(i+1, j, 1)) movesWhite.erase((i+1)*SIZE + j);
            if (isSuicide(i+1, j, 0)) movesBlack.erase((i+1)*SIZE + j);
            if (isSuicide(i+1, j, 1)) movesWhite.erase((i+1)*SIZE + j);
        }
        else if (board[i+1][j].type == 0 &&
                 (board[i+1][j].group->liberties.size()) == 1) {
            BoardPos * lib = *((board[i+1][j].group->liberties).begin());
            movesBlack.erase((lib->i)*SIZE + lib->j);
            movesWhite.insert((lib->i)*SIZE + lib->j);
        }
    }
    if (j+1 < SIZE) {
        if (board[i][j+1].type > 2) {
            if (isEye(i, j+1, 1)) movesWhite.erase(i*SIZE + j+1);
            if (isSuicide(i, j+1, 0)) movesBlack.erase(i*SIZE + j+1);
            if (isSuicide(i, j+1, 1)) movesWhite.erase(i*SIZE + j+1);
        }
        else if (board[i][j+1].type == 0 &&
                 (board[i][j+1].group->liberties.size()) == 1) {
            BoardPos * lib = *((board[i][j+1].group->liberties).begin());
            movesBlack.erase((lib->i)*SIZE + lib->j);
            movesWhite.insert((lib->i)*SIZE + lib->j);
        }
    }
    if (i > 0 && j > 0 && board[i-1][j-1].type > 2 && 
        isEye(i-1, j-1, 1)) {
        movesWhite.erase((i-1)*SIZE + j-1);
    }
    if (i > 0 && j+1 < SIZE && board[i-1][j+1].type > 2 && 
        isEye(i-1, j+1, 1)) {
        movesWhite.erase((i-1)*SIZE + j+1);
    }
    if (i+1 < SIZE && j > 0 && board[i+1][j-1].type > 2 && 
        isEye(i+1, j-1, 1)) {
        movesWhite.erase((i+1)*SIZE + j-1);
    }
    if (i+1 < SIZE && j+1 < SIZE && board[i+1][j+1].type > 2 && 
        isEye(i+1, j+1, 1)) {
        movesWhite.erase((i+1)*SIZE + j+1);
    }
}

bool GoBoard::nextMove(GoMove& mov) {
    if (DEBUG) std::cerr << "NextMove" << ' ' << (int)mov.i << ' ' << (int)mov.j << std::endl;
    It it = setKo.begin();
    while (it != setKo.end()) {
        It act = it++;
        if ((*act)->type == 8) (*act)->type = 7;
        else if ((*act)->type == 7) {
            int i2 = (*act)->i;
            int j2 = (*act)->j;
            if (!isSuicide(i2, j2, 0) && !isEye(i2, j2, 0)) {
                movesBlack.insert(i2*SIZE + j2);
            }
            if (!isSuicide(i2, j2, 1) && !isEye(i2, j2, 1)) {
                movesWhite.insert(i2*SIZE + j2);
            }
            (*act)->type = 9;
            setKo.erase(act);
        };
    }
    if (mov.i >= 0 && mov.j >= 0) {
        killAdjacent(mov.i, mov.j, mov.type);
        addToGroups(mov.i, mov.j, mov.type);
        board[mov.i][mov.j].type = mov.type;
        if (mov.type == 0) updateMovesBlack(mov.i, mov.j);
        else updateMovesWhite(mov.i, mov.j);
        movesBlack.erase(mov.i*SIZE + mov.j);
        movesWhite.erase(mov.i*SIZE + mov.j);
        hashAct ^= zob[mov.i][mov.j][2];
        hashAct ^= zob[mov.i][mov.j][mov.type];
        if (hashes.find(hashAct) != hashes.end()) return false;
        hashes.insert(hashAct);
    }
    return true;
}

char GoBoard::randomGame(int move, char color) {
    GoMove mov(move/SIZE, move%SIZE, color, true);
    nextMove(mov);
    int turn = 1;
    int pass = 0;
    char col = (color+turn)%2;
    bool norepeat = true;
    while (pass < 2 and norepeat and turn < 300) {
        if (col == 0 && movesBlack.size() == 0) pass++;
        else if (col == 1 && movesWhite.size() == 0) pass++;
        else {
            std::set<int>::const_iterator it;
            int ind;
            if (col == 0) {
                ind = rand()%movesBlack.size();
                it = movesBlack.begin();
            }
            else {
                ind = rand()%movesWhite.size();
                it = movesWhite.begin();
            }
            advance(it,ind);
            int move = *it;
            mov.i = move/SIZE;
            mov.j = move%SIZE;
            mov.type = col;
            norepeat = nextMove(mov);
            printBoard();
            printGroups();
            printMoves();
            pass = 0;
        }
        turn++;
        col = (color+turn)%2;
    }
    computeScore();
    if (color == 0) return scoreBlack > scoreWhite;
    else return scoreBlack < scoreWhite;
}

bool GoBoard::isEye(char i, char j, char color) {
    if (i > 0 && (board[i-1][j].type != color || 
        board[i-1][j].group->liberties.size() == 1)) return false;
    if (j > 0 && (board[i][j-1].type != color ||
        board[i][j-1].group->liberties.size() == 1)) return false;
    if (i+1 < SIZE && (board[i+1][j].type != color ||
        board[i+1][j].group->liberties.size() == 1)) return false;
    if (j+1 < SIZE && (board[i][j+1].type != color ||
        board[i][j+1].group->liberties.size() == 1)) return false;
    char num = 0;
    if (i-1 < 0 || j-1 < 0 || board[i-1][j-1].type == color) num++;
    if (i-1 < 0 || j+1 >= SIZE || board[i-1][j+1].type == color) num++;
    if (i+1 >= SIZE || j-1 < 0 || board[i+1][j-1].type == color) num++;
    if (i+1 >= SIZE || j+1 >= SIZE || board[i+1][j+1].type == color) num++;
    if (num < 3) return false;
    if (DEBUG) std::cerr << "Its eye" << std::endl;
    return true;
}

bool GoBoard::isLegal(char i, char j, char color) {
    if (i < 0 or j < 0 or i >= SIZE or j >= SIZE or
        !board[i][j].isEmpty2() or isSuicide(i, j, color)) return false;
    return true;
}

int GoBoard::countArea(char i, char j, char color) {
    if (board[i][j].type > 5) {
        board[i][j].type = 5;
        int res = 1;
        if (i > 0 and board[i-1][j].type != 5) res += countArea(i-1, j, color);
        if (j > 0 and board[i][j-1].type != 5) res += countArea(i, j-1, color);
        if (i+1 < SIZE and board[i+1][j].type != 5) res += countArea(i+1, j, color);
        if (j+1 < SIZE and board[i][j+1].type != 5) res += countArea(i,j+1, color);
        return res;
    }
    else if (board[i][j].type == color) return 0;
    else return -9999;
}

void GoBoard::computeScore() {
    scoreBlack = 0;
    scoreWhite = 0;
    for (ItG it = bGroups.begin(); it != bGroups.end(); ++it) {
        StoneGroup* gr = *it;
        for (It it2 = (gr->liberties).begin(); it2 != (gr->liberties).end(); ++it2) {    
            BoardPos* ps = *it2;
            if (ps->type == 9) {
                int res = countArea(ps->i, ps->j, 0);
                if (res > 0) scoreBlack += res;
            }
        }
        scoreBlack += (gr->stones).size();
    }
    for (ItG it = wGroups.begin(); it != wGroups.end(); ++it) {
        StoneGroup* gr = *it;
        for (It it2 = (gr->liberties).begin(); it2 != (gr->liberties).end(); ++it2) {    
            BoardPos* ps = *it2;
            if (ps->type == 9) {
                int res = countArea(ps->i, ps->j, 1);
                if (res > 0) scoreWhite += res; 
            }
        }
        scoreWhite += (gr->stones).size();
    }
    scoreWhite += komi;
}

void GoBoard::printBoard() {
    std::ofstream flog;
    flog.open("log.txt", std::ios::app);
    flog << std::endl;
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            if (board[i][j].isBlack2()) flog << 'X';
            else if (board[i][j].isWhite2()) flog << 'O';
            else if (board[i][j].koMark()) flog << ":";
            else flog << ".";
        }
        flog << std::endl;
    }
    flog << "Black Cap: " << capBlack << 
                 "  White Cap: " << capWhite << std::endl;
}

void GoBoard::printScore() {
    std::ofstream flog;
    flog.open("log.txt", std::ios::app);
    flog << std::endl << "Black score: " << scoreBlack << std::endl
         << "White score: " << scoreWhite << std::endl;
    
}

void GoBoard::printGroups() {
    std::ofstream flog;
    flog.open("log.txt", std::ios::app);
    flog << "black: " << bGroups.size() << std::endl;
    for (ItG it = bGroups.begin(); it != bGroups.end(); ++it) {
        StoneGroup* gr = *it;
        flog << "Stones: " << (gr->stones).size() << std::endl;
        for (It it2 = (gr->stones).begin(); it2 != (gr->stones).end(); ++it2) {    
            BoardPos* ps = *it2;
            flog << (int)(*ps).i << ',' << (int)(*ps).j << ' ';
        }
        flog << std::endl << "Liberties: " << (gr->liberties).size() << std::endl;
        for (It it2 = (gr->liberties).begin(); it2 != (gr->liberties).end(); ++it2) {    
            BoardPos* ps = *it2;
            flog << (int)(*ps).i << ',' << (int)(*ps).j << ' ';
        }
        flog << std::endl;
    }
    flog << std::endl;
    flog << "white: " << wGroups.size() << std::endl;
    for (ItG it = wGroups.begin(); it != wGroups.end(); ++it) {
        StoneGroup* gr = *it;
        flog << "Stones: " << (gr->stones).size() << std::endl;
        for (It it2 = (gr->stones).begin(); it2 != (gr->stones).end(); ++it2) {    
            BoardPos* ps = *it2;
            flog << (int)(*ps).i << ',' << (int)(*ps).j << ' ';
        }
        flog << std::endl << "Liberties: " << (gr->liberties).size() << std::endl;
        for (It it2 = (gr->liberties).begin(); it2 != (gr->liberties).end(); ++it2) {    
            BoardPos* ps = *it2;
            flog << (int)(*ps).i << ',' << (int)(*ps).j << ' ';
        }
        flog << std::endl;
    }
    flog << std::endl;
}

void GoBoard::printMoves() {
    std::ofstream flog;
    flog.open("log.txt", std::ios::app);
    flog << "black Moves: " << movesBlack.size() << std::endl;
    for (ItI it = movesBlack.begin(); it != movesBlack.end(); ++it) {
        int p = *it;
        flog << p/SIZE << ',' << p%SIZE << ' ';
    }
    flog << std::endl << std::endl;
    flog << "white Moves: " << movesWhite.size() << std::endl;
    for (ItI it = movesWhite.begin(); it != movesWhite.end(); ++it) {
        int p = *it;
        flog << p/SIZE << ',' << p%SIZE << ' ';
    }
    flog << std::endl << std::endl;
}
