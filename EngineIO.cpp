/* 
 * File:   EngineIO.cpp
 * Author: Red
 * 
 * Created on 22 de octubre de 2012, 22:04
 */

#include <iostream>
#include "EngineIO.h"

EngineIO::EngineIO() {
}

EngineIO::EngineIO(char c) {
    color = c;
}

EngineIO::EngineIO(const EngineIO& orig) {
}

EngineIO::~EngineIO() {
}

GoMove EngineIO::nextMove(GoBoard& board) {
    int i = -10;
    int j = -10;
    bool finish = false;
    while(!finish) {
        std::cin >> i >> j;
        if (i == -1 && j == -1) finish = true;
        else if (!board.isLegal(i, j, color)) std::cout << "Invalid" << std::endl;
        else finish = true;
    }
    if (i != -1) return GoMove(i, j, color, true);
    return GoMove(-1,-1,-1,false);
}
