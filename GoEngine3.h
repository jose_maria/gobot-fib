/* 
 * File:   GoEngine3.h
 * Author: Red
 *
 * Created on 15 de abril de 2013, 20:39
 */

#ifndef GOENGINE3_H
#define	GOENGINE3_H

#include <vector>
#include "GoMove.h"
#include "GoBoard3.h"

class GoEngine3 {
public:
    
    GoEngine3();
    GoEngine3(const GoEngine3& orig);
    virtual ~GoEngine3();
    virtual GoMove nextMove(GoBoard3& board, char color);
    
protected:
    
};

#endif	/* GOENGINE3_H */

