#include <stdlib.h>
#include "GoBoard2.h"

GoBoard2::GoBoard2() {
}

GoBoard2::GoBoard2(char x) {
    SIZE = x;
    SIZE2 = x*x;
    clearBoard();
}

GoBoard2::GoBoard2(const GoBoard2& orig) {
    SIZE = orig.SIZE;
    SIZE2 = orig.SIZE2;
    capWhite = orig.capWhite;
    capBlack = orig.capBlack;
    scoreWhite = orig.scoreWhite;
    scoreBlack = orig.scoreWhite;
    lastMove = orig.lastMove;
    totturn = orig.totturn;
    komi = orig.komi;
    zob = orig.zob;
    hashAct = hashAct;
    hashes = orig.hashes;
    freeMoves = orig.freeMoves;
    setKo = orig.setKo;
    board = Mat(SIZE, Vec(SIZE));
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            board[i][j].type = orig.board[i][j].type;
            board[i][j].i = orig.board[i][j].i;
            board[i][j].j = orig.board[i][j].j;
        }
    }
    for (ItG it = orig.bGroups.begin(); it != orig.bGroups.end(); ++it) {
        StoneGroup* newg = new StoneGroup();
        StoneGroup* gr = *it;
        newg->color = gr->color;
        for (It it2 = (gr->stones).begin(); it2 != (gr->stones).end(); ++it2) {    
            BoardPos* ps = *it2;
            newg->addStone(&board[ps->i][ps->j]);
            board[ps->i][ps->j].group = newg;
        };
        for (It it2 = (gr->liberties).begin(); it2 != (gr->liberties).end(); ++it2) {    
            BoardPos* ps = *it2;
            (newg->liberties).insert(&board[ps->i][ps->j]);
        };
        bGroups.insert(newg);
    }
    for (ItG it = orig.wGroups.begin(); it != orig.wGroups.end(); ++it) {
        StoneGroup* newg = new StoneGroup();
        StoneGroup* gr = *it;
        newg->color = gr->color;
        for (It it2 = (gr->stones).begin(); it2 != (gr->stones).end(); ++it2) {    
            BoardPos* ps = *it2;
            newg->addStone(&board[ps->i][ps->j]);
            board[ps->i][ps->j].group = newg;
        };
        for (It it2 = (gr->liberties).begin(); it2 != (gr->liberties).end(); ++it2) {    
            BoardPos* ps = *it2;
            (newg->liberties).insert(&board[ps->i][ps->j]);
        };
        wGroups.insert(newg);
    }
}

GoBoard2::~GoBoard2() {
    for (ItG it = bGroups.begin(); it != bGroups.end(); ++it) {
        StoneGroup* gr = *it;
        delete gr;
    }
    for (ItG it = wGroups.begin(); it != wGroups.end(); ++it) {
        StoneGroup* gr = *it;
        delete gr;
    }
}

void GoBoard2::initZob() {
    zob = VZob(SIZE, MChar(SIZE, VChar(3, 2)));
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            zob[i][j][0] = rand();
            zob[i][j][1] = rand();
            zob[i][j][2] = rand();
        }
    }
}

int GoBoard2::hashZob() {
    int key = 0;
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            int stat = board[i][j].type;
            if (stat > 2) stat = 2;
            key ^= zob[i][j][stat];
        }
    }
    return key;
}

void GoBoard2::clearBoard() {
    initZob();
    hashes.clear();
    freeMoves.clear();
    bGroups.clear();
    wGroups.clear();
    setKo.clear();
    komi = 6.5;
    capWhite = 0;
    capBlack = 0;
    scoreWhite = 0;
    scoreBlack = 0;
    totturn = 1;
    lastMove = -1;
    board = Mat(SIZE, Vec(SIZE));
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            board[i][j].group = 0;
            board[i][j].type = 9;
            board[i][j].i = i;
            board[i][j].j = j;
            freeMoves.insert(i*SIZE + j);
        }
    }
    hashAct = hashZob();
    hashes.insert(hashAct);
}

void GoBoard2::setKomi(double k) {
    komi = k;
}

void GoBoard2::fusionGroups(StoneGroup * g1, StoneGroup * g2) {
    if ((*g1).color == 0) {
        bGroups.erase(g1);
        bGroups.erase(g2);
    }
    else {
        wGroups.erase(g1);
        wGroups.erase(g2);
    }
    if (DEBUG) std::cerr << "fusionGroups" << ' ' << g1 << ' ' << g2 << std::endl;
    for (It it = (g2->stones).begin(); it != (g2->stones).end(); ++it) {
        if (DEBUG) std::cerr << (int)(**it).i << ',' << (int)(**it).j << ' ';
        (**it).group = g1;
        g1->addStone(*it);
    }
    g1->liberties.insert((g2->liberties).begin(), (g2->liberties).end());
    if (DEBUG) std::cerr << std::endl;
    delete g2;
    if ((*g1).color == 0) bGroups.insert(g1);
    else wGroups.insert(g1);
}

StoneGroup* GoBoard2::createGroup(char i, char j, char color) {
    StoneGroup* g = new StoneGroup(&board[i][j], color);
    if (i > 0 && board[i-1][j].type >= 2) {
        (g->liberties).insert(&board[i-1][j]);
    }
    if (j > 0 && board[i][j-1].type >= 2) {
        (g->liberties).insert(&board[i][j-1]);
    }
    if (i+1 < SIZE && board[i+1][j].type >= 2) {
        (g->liberties).insert(&board[i+1][j]);
    }
    if (j+1 < SIZE && board[i][j+1].type >= 2) {
        (g->liberties).insert(&board[i][j+1]);
    }
    if (color == 0) bGroups.insert(g);
    else wGroups.insert(g);
    return g;
}

void GoBoard2::addToGroups(char i, char j, char color) {
    if (DEBUG) std::cerr << "CreateGroup" << std::endl;
    StoneGroup* group = createGroup(i, j, color);
    board[i][j].group = group;
    if (i > 0) {
        if (board[i-1][j].type == color) {
            if (board[i-1][j].group != group) {
                fusionGroups(board[i][j].group, board[i-1][j].group);
                board[i-1][j].group = board[i][j].group;
            }
        }
        if (board[i-1][j].type < 2) {
            (board[i-1][j].group->liberties).erase(&board[i][j]);
        }
    }
    if (j > 0) {
        if (board[i][j-1].type == color) {
            if (board[i][j-1].group != group) {
                fusionGroups(board[i][j].group, board[i][j-1].group);
                board[i][j-1].group = board[i][j].group;
            }
        }
        if (board[i][j-1].type < 2) {
            (board[i][j-1].group->liberties).erase(&board[i][j]);
        }
    }
    if (i+1 < SIZE) {
        if (board[i+1][j].type == color) {
            if (board[i+1][j].group != group) {
                fusionGroups(board[i][j].group, board[i+1][j].group);
                board[i+1][j].group = board[i][j].group;
            }
        }
        if (board[i+1][j].type < 2) {
            (board[i+1][j].group->liberties).erase(&board[i][j]);
        }
    }
    if (j+1 < SIZE) {
        if (board[i][j+1].type == color) {
            if (board[i][j+1].group != group) {
                fusionGroups(board[i][j].group, board[i][j+1].group);
                board[i][j+1].group = board[i][j].group;
            }
        }
        if (board[i][j+1].type < 2) {
            (board[i][j+1].group->liberties).erase(&board[i][j]);
        }
    }
}

bool GoBoard2::isSuicide(char i, char j, char color) {
    if (DEBUG) std::cerr << "isSuicide " << (int)i << ',' << (int)j << ' '
                         << int(color) << std::endl;
    char color2;
    if (color == 0) color2 = 1;
    else color2 = 0;
    
    if ((i-1 >= 0 && board[i-1][j].type == color2 && 
        (board[i-1][j].group->liberties).size() == 1) ||
        (j-1 >= 0 && board[i][j-1].type == color2 && 
        (board[i][j-1].group->liberties).size() == 1) ||
        (i+1 < SIZE && board[i+1][j].type == color2 && 
        (board[i+1][j].group->liberties).size() == 1) ||
        (j+1 < SIZE && board[i][j+1].type == color2 && 
        (board[i][j+1].group->liberties).size() == 1)) {
                if (DEBUG) std::cerr << "2if" << std::endl;
                return false;
    }
    
    if ((i-1 < 0 || board[i-1][j].type == color2) &&
        (j-1 < 0 || board[i][j-1].type == color2) && 
        (i+1 >= SIZE || board[i+1][j].type == color2) && 
        (j+1 >= SIZE || board[i][j+1].type == color2)) { 
                if (DEBUG) std::cerr << "1if" << std::endl;
                return true;
    }
    
    // check if it suicides a group
    // if its next to a group with 1 liberty and it cant connect with
    // a group with more than 1 liberty, its a suicide move
    if ((i-1 >= 0 && board[i-1][j].type == color && 
        (board[i-1][j].group->liberties).size() == 1) &&
        !((j-1 >= 0 && board[i][j-1].type > 2) ||
         (i+1 < SIZE && board[i+1][j].type > 2) ||
         (j+1 < SIZE && board[i][j+1].type > 2) ||
         (j-1 >= 0 && board[i][j-1].type == color
          && (board[i][j-1].group->liberties).size() > 1) ||
         (i+1 < SIZE && board[i+1][j].type == color
          && (board[i+1][j].group->liberties).size() > 1) ||
         (j+1 < SIZE && board[i][j+1].type == color
          && (board[i][j+1].group->liberties).size() > 1))) {
                return true;
    }
    if  ((j-1 >= 0 && board[i][j-1].type == color && 
        (board[i][j-1].group->liberties).size() == 1) &&
        !((i-1 >= 0 && board[i-1][j].type > 2) ||
         (i+1 < SIZE && board[i+1][j].type > 2) ||
         (j+1 < SIZE && board[i][j+1].type > 2) ||
         (i-1 >= 0 && board[i-1][j].type == color
          && (board[i-1][j].group->liberties).size() > 1) ||
         (i+1 < SIZE && board[i+1][j].type == color
          && (board[i+1][j].group->liberties).size() > 1) ||
         (j+1 < SIZE && board[i][j+1].type == color
          && (board[i][j+1].group->liberties).size() > 1))) {
                return true;
    }
    
    if  ((i+1 < SIZE && board[i+1][j].type == color && 
        (board[i+1][j].group->liberties).size() == 1) && 
        !((i-1 >= 0 && board[i-1][j].type > 2) ||
         (j-1 >= 0 && board[i][j-1].type > 2) ||
         (j+1 < SIZE && board[i][j+1].type > 2) ||
         (i-1 >= 0 && board[i-1][j].type == color
          && (board[i-1][j].group->liberties).size() > 1) ||
         (j-1 >= 0 && board[i][j-1].type == color
          && (board[i][j-1].group->liberties).size() > 1) ||
         (j+1 < SIZE && board[i][j+1].type == color
          && (board[i][j+1].group->liberties).size() > 1))) {
                return true;
    }
    if  ((j+1 < SIZE && board[i][j+1].type == color && 
        (board[i][j+1].group->liberties).size() == 1) &&
        !((i-1 >= 0 && board[i-1][j].type > 2) ||
         (j-1 >= 0 && board[i][j-1].type > 2) ||
         (i+1 < SIZE && board[i+1][j].type > 2) ||
         (i-1 >= 0 && board[i-1][j].type == color
          && (board[i-1][j].group->liberties).size() > 1) ||
         (j-1 >= 0 && board[i][j-1].type == color
          && (board[i][j-1].group->liberties).size() > 1) ||
         (i+1 < SIZE && board[i+1][j].type == color
          && (board[i+1][j].group->liberties).size() > 1))) {
                return true;
    }
    return false;
}

void GoBoard2::killStone(BoardPos * pos) {
    if (DEBUG) std::cerr << "killStone" << std::endl;
    char color = pos->type;
    if (color == 0) color = 1;
    else color = 0;
    int i = pos->i;
    int j = pos->j;
    if (i > 0 && board[i-1][j].type == color) {
        (board[i-1][j].group->liberties).insert(&board[i][j]);
    }
    if (j > 0 && board[i][j-1].type == color) {
        (board[i][j-1].group->liberties).insert(&board[i][j]);
    }
    if (i+1 < SIZE && board[i+1][j].type == color) {
        (board[i+1][j].group->liberties).insert(&board[i][j]);
    }
    if (j+1 < SIZE && board[i][j+1].type == color) {
        (board[i][j+1].group->liberties).insert(&board[i][j]);
    }
    hashAct ^= zob[i][j][board[i][j].type];
    hashAct ^= zob[i][j][2];
    board[i][j].type = 9;
    board[i][j].group = 0;
    freeMoves.insert(i*SIZE + j);
}

bool GoBoard2::killGroup(StoneGroup * g) {
    if (DEBUG) std::cerr << "killGroup" << std::endl;
    int tam = (g->stones).size();
    bool ko = false;
    if (tam == 1) {
        BoardPos * p = *(g->stones).begin();
        killStone(p);
        ko = true;
    }
    else {
        for (It it = (g->stones).begin(); it != (g->stones).end(); ++it) {
            BoardPos * p = *it;
            killStone(p);
        }
    }
    if (g->color == 0) {
        capWhite += tam;
        bGroups.erase(g);
    }
    else {
        capBlack += tam;
        wGroups.erase(g);
    }
    delete g;
    return ko;
}

void GoBoard2::killAdjacent(char i, char j, char color) {
    if (DEBUG) std::cerr << "killAdjacent" << std::endl;
    int color2 = 0;
    if (color == 0) color2 = 1;
    if (i > 0 && board[i-1][j].type == color2 && 
        (board[i-1][j].group->liberties).size() == 1) {
        if (killGroup(board[i-1][j].group)) {
            if ((j <= 0 || board[i][j-1].type == color2) && 
               (i+1 >= SIZE || board[i+1][j].type == color2) && 
               (j+1 >= SIZE || board[i][j+1].type == color2)) {
                setKo.insert((i-1)*SIZE + j);
                freeMoves.erase((i-1)*SIZE + j);
                board[i-1][j].type = 8;
            }
        }
    }
    if (j > 0 && board[i][j-1].type == color2 && 
        (board[i][j-1].group->liberties).size() == 1) {
        if (killGroup(board[i][j-1].group)) {
            if ((i <= 0 || board[i-1][j].type == color2) &&
               (i+1 >= SIZE || board[i+1][j].type == color2) && 
               (j+1 >= SIZE || board[i][j+1].type == color2)) {
                setKo.insert(i*SIZE + j-1);
                freeMoves.erase(i*SIZE + j-1);
                board[i][j-1].type = 8;
            }
        }
    }
    if (i+1 < SIZE && board[i+1][j].type == color2 && 
        (board[i+1][j].group->liberties).size() == 1) {
        if (killGroup(board[i+1][j].group)) {
            if ((i <= 0 || board[i-1][j].type == color2) &&
               (j <= 0 || board[i][j-1].type == color2) && 
               (j+1 >= SIZE || board[i][j+1].type == color2)) {
                setKo.insert((i+1)*SIZE + j);
                freeMoves.erase((i+1)*SIZE + j);
                board[i+1][j].type = 8;
            }
        }
    }
    if (j+1 < SIZE && board[i][j+1].type == color2 && 
        (board[i][j+1].group->liberties).size() == 1) {
        if (killGroup(board[i][j+1].group)) {
            if ((i <= 0 || board[i-1][j].type == color2) &&
               (j <= 0 || board[i][j-1].type == color2) && 
               (i+1 >= SIZE || board[i+1][j].type == color2)) {
                setKo.insert(i*SIZE + j+1);
                freeMoves.erase(i*SIZE + j+1);
                board[i][j+1].type = 8;
            }
        }
    }
}

bool GoBoard2::nextMove(GoMove& mov) {
    //std::ofstream flog;
    //flog.open("log.txt", std::ios::app);
    //flog << (int)mov.i << ' ' << (int)mov.j << ' ' << (int)mov.type << std::endl;
    //flog << "beforeko" << std::endl;
    ItI it = setKo.begin();
    while (it != setKo.end()) {
        int iact = *it/SIZE;
        int jact = *it%SIZE;
        it++;
        if (board[iact][jact].type == 8) board[iact][jact].type = 7;
        else if (board[iact][jact].type == 7) {
            freeMoves.insert(iact*SIZE + jact);
            board[iact][jact].type = 9;
            setKo.erase(iact*SIZE + jact);
        }
    }
    lastMove = -1;
    totturn++;
    if (mov.i >= 0 && mov.j >= 0) {
        //flog << "beforekill" << std::endl;
        killAdjacent(mov.i, mov.j, mov.type);
        //flog << "beforeadd" << std::endl;
        addToGroups(mov.i, mov.j, mov.type);
        //flog << "done" << std::endl;
        board[mov.i][mov.j].type = mov.type;
        freeMoves.erase(mov.i*SIZE + mov.j);
        hashAct ^= zob[mov.i][mov.j][2];
        hashAct ^= zob[mov.i][mov.j][mov.type];
        if (hashes.find(hashAct) != hashes.end()) return false;
        hashes.insert(hashAct);
        //printBoard();
        //printGroups();
        //printMoves();
        lastMove = mov.i*SIZE + mov.j;
        
    }
    return true;
}

char GoBoard2::randomGameUCT(char color) {
    int pass = 0;
    int move;
    int turn = 1;
    bool col = color;
    bool norepeat = true;
    GoMove mov(0, 0, 0, true);
    while (pass < 2 and norepeat and turn < 300) {
        move = getRandomMove(col);
        if (move == -1) {
            mov.i = -1;
            mov.j = -1;
            nextMove(mov);
            pass++;
        }
        else {
            mov.i = move/SIZE;
            mov.j = move%SIZE;
            mov.type = col;
            norepeat = nextMove(mov);
            pass = 0;
        }
        turn++;
        col = !col;
    }
    computeScore();
    if (color == 0) return scoreBlack > scoreWhite;
    else return scoreBlack < scoreWhite;
}

char GoBoard2::randomGameMCTS(int move, char color) {
    GoMove mov;
    int pass = 0;
    if (move >= 0) mov = GoMove(move/SIZE, move%SIZE, color, true);
    else {
        pass = 1;
        mov = GoMove(-1, -1, color, true);
    }
    nextMove(mov);
    int turn = 1;
    bool col = !color;
    bool norepeat = true;
    while (pass < 2 and norepeat and turn < 300) {
        move = getRandomMove(col);
        if (move == -1) {
            mov.i = -1;
            mov.j = -1;
            nextMove(mov);
            pass++;
        }
        else {
            mov.i = move/SIZE;
            mov.j = move%SIZE;
            mov.type = col;
            norepeat = nextMove(mov);
            pass = 0;
        }
        turn++;
        col = !col;
    }
    computeScore();
    if (color == 0) return scoreBlack > scoreWhite;
    else return scoreBlack < scoreWhite;
}

int GoBoard2::getRandomMove(char color) {
    //std::ofstream flog;
    //flog.open("log.txt", std::ios::app);
    if (lastMove >= 0) {
        int i = lastMove/SIZE;
        int j = lastMove%SIZE;
        int p = rand()%1000;
        if (p < P_1LIB) {
            if (board[i][j].group->liberties.size() == 1) {
                BoardPos * pos = *board[i][j].group->liberties.begin();
                if (pos->type == 9) return ((pos->i)*SIZE + pos->j);
            }
            int moves[SIZE2];
            int ind = 0;
            // Try to save group by capturing not implemented
            if (i > 0 && board[i-1][j].type == color && 
                board[i-1][j].group->liberties.size() == 1) {
                BoardPos * pos = *board[i-1][j].group->liberties.begin();
                if (pos->type == 9 && !isSuicide(pos->i, pos->j, color)) {
                    moves[ind++] = (pos->i*SIZE) + pos->j;
                }
            }
           if (j > 0 && board[i][j-1].type == color && 
               board[i][j-1].group->liberties.size() == 1) {
                BoardPos * pos = *board[i][j-1].group->liberties.begin();
                if (pos->type == 9 && !isSuicide(pos->i, pos->j, color)) {
                    moves[ind++] = (pos->i*SIZE) + pos->j;
                }
            }
            if (i+1 < SIZE && board[i+1][j].type == color &&
                board[i+1][j].group->liberties.size() == 1) {
                BoardPos * pos = *board[i+1][j].group->liberties.begin();
                if (pos->type == 9 && !isSuicide(pos->i, pos->j, color)) {
                    moves[ind++] = (pos->i*SIZE) + pos->j;
                }
            }
            if (j+1 < SIZE && board[i][j+1].type == color &&
                board[i][j+1].group->liberties.size() == 1) {
                BoardPos * pos = *board[i][j+1].group->liberties.begin();
                if (pos->type == 9 && !isSuicide(pos->i, pos->j, color)) {
                    moves[ind++] = (pos->i*SIZE) + pos->j;
                }
            }
            if (ind > 0) return moves[rand()%ind];
        }
        p = rand()%1000;
        if (p < P_2LIB) {
            if (board[i][j].group->liberties.size() == 2) {
                It it = board[i][j].group->liberties.begin();
                BoardPos * pos1 = *it;
                it++;
                BoardPos * pos2 = *it;
                if (pos1->type == 9 && !isSuicide(pos1->i, pos1->j, color)) {
                    if (pos2->type != 9 || isSuicide(pos2->i, pos2->j, color)) {
                        return ((pos1->i)*SIZE + pos1->j);
                    }
                    if (p&2 == 0) return ((pos1->i)*SIZE + pos1->j);
                    return ((pos2->i)*SIZE + pos2->j);
                }
                else if (pos2->type == 9 && !isSuicide(pos2->i,pos2->j,color)) {
                    return ((pos2->i)*SIZE + pos2->j);
                }
            }
            int moves[SIZE2];
            int ind = 0;
            // Try to save group by putting in atari neighbour not implemented
            if (i > 0 && board[i-1][j].type == color && 
                board[i-1][j].group->liberties.size() == 2) {
                It it = board[i-1][j].group->liberties.begin();
                BoardPos * pos = *it;
                moves[ind++] = (pos->i)*SIZE + pos->j;
                it++;
                pos = *it;
                moves[ind++] = (pos->i)*SIZE + pos->j;
            }
            if (j > 0 && board[i][j-1].type == color &&
                board[i][j-1].group->liberties.size() == 2) {
                It it = board[i][j-1].group->liberties.begin();
                BoardPos * pos = *it;
                moves[ind++] = (pos->i)*SIZE + pos->j;
                it++;
                pos = *it;
                moves[ind++] = (pos->i)*SIZE + pos->j;
            }
            if (i+1 < SIZE && board[i+1][j].type == color &&
                board[i+1][j].group->liberties.size() == 2) {
                It it = board[i+1][j].group->liberties.begin();
                BoardPos * pos = *it;
                moves[ind++] = (pos->i)*SIZE + pos->j;
                it++;
                pos = *it;
                moves[ind++] = (pos->i)*SIZE + pos->j;
            }
            if (j+1 < SIZE && board[i][j+1].type == color &&
                board[i][j+1].group->liberties.size() == 2) {
                It it = board[i][j+1].group->liberties.begin();
                BoardPos * pos = *it;
                moves[ind++] = (pos->i)*SIZE + pos->j;
                it++;
                pos = *it;
                moves[ind++] = (pos->i)*SIZE + pos->j;
            }
        }
        p = rand()%1000;
        if (p < P_PATTERN) {

        }
    }
    int moves[SIZE2];
    int ind = 0;
    if ((color == 0 && totturn <= 3) || (color == 1 && totturn <= 4)) {
        for (int i = 2; i < SIZE-2; ++i) {
            for (int j = 2; j < SIZE-2; ++j) {
                if (board[i][j].type == 9 && !isSuicide(i, j, color) && 
                    !isEye(i, j, color))   moves[ind++] = i*SIZE + j;
            }
        }
        return moves[rand()%ind];       
    }
    //flog << "turn:" << totturn << " col:" << int(color) << " moves: ";
    //printBoard();
    moves[ind++] = -1;
    for (ItI it = freeMoves.begin(); it != freeMoves.end(); ++it) {
        int t = *it;
        int i = t/SIZE;
        int j = t%SIZE;
        //flog << i << ',' << j;
        if (!isSuicide(i, j, color) && !isEye(i, j, color)) moves[ind++] = t;
            //else flog << " eye" << std::endl;
        //}
        //else flog << " suicide" << std::endl;
    }
    if (ind == 0) return -1;
    return moves[rand()%ind];
}

VChar GoBoard2::getAllMoves(char color) {
    VChar moves(0);
    int ind = 0;
    for (ItI it = freeMoves.begin(); it != freeMoves.end(); ++it) {
        int t = *it;
        int i = t/SIZE;
        int j = t%SIZE;
        //flog << i << ',' << j;
        if (!isSuicide(i, j, color) && !isEye(i, j, color)) moves.push_back(t);
            //else flog << " eye" << std::endl;
        //}
        //else flog << " suicide" << std::endl;
    }
    return moves;
}

bool GoBoard2::isEye(char i, char j, char color) {
    if ((i > 0 && (board[i-1][j].type != color ||
        board[i-1][j].group->liberties.size() == 1)) ||
        (j > 0 && (board[i][j-1].type != color ||
        board[i][j-1].group->liberties.size() == 1)) ||
        (i+1 < SIZE && (board[i+1][j].type != color ||
        board[i+1][j].group->liberties.size() == 1)) ||
        (j+1 < SIZE && (board[i][j+1].type != color ||
        board[i][j+1].group->liberties.size() == 1))) return false;
    if (i == 0 || i == SIZE-1 || j == 0 || j == SIZE-1) {
        char col = 0;
        if (color == 0) col = 1;
        if ((i > 0 && j > 0 && board[i-1][j-1].type == col) || 
            (i+1 < SIZE && j > 0 && board[i+1][j-1].type == col) || 
            (i > 0 && j+1 < SIZE && board[i-1][j+1].type == col) || 
            (i+1 < SIZE && j+1 < SIZE && board[i+1][j+1].type == col)) {
            return false;
        }
    }
    char n = (i <= 0 || j <= 0 || board[i-1][j-1].type == color);
    n += (i <= 0 || j+1 >= SIZE || board[i-1][j+1].type == color);
    n += (i+1 >= SIZE || j <= 0 || board[i+1][j-1].type == color);
    n += (i+1 >= SIZE || j+1 >= SIZE || board[i+1][j+1].type == color);
    return n > 2;
}

bool GoBoard2::isLegal(char i, char j, char color) {
    if (i < 0 or j < 0 or i >= SIZE or j >= SIZE or
        !board[i][j].isEmpty2() or isSuicide(i, j, color)) return false;
    return true;
}

int GoBoard2::countArea(char i, char j, char color) {
    if (board[i][j].type > 5) {
        board[i][j].type = 5;
        int res = 1;
        if (i > 0 and board[i-1][j].type != 5) res += countArea(i-1, j, color);
        if (j > 0 and board[i][j-1].type != 5) res += countArea(i, j-1, color);
        if (i+1 < SIZE and board[i+1][j].type != 5) res += countArea(i+1, j, color);
        if (j+1 < SIZE and board[i][j+1].type != 5) res += countArea(i,j+1, color);
        return res;
    }
    else if (board[i][j].type == color) return 0;
    else return -9999;
}

void GoBoard2::computeScore() {
    scoreBlack = 0;
    scoreWhite = 0;
    for (ItG it = bGroups.begin(); it != bGroups.end(); ++it) {
        StoneGroup* gr = *it;
        for (It it2 = (gr->liberties).begin(); it2 != (gr->liberties).end(); ++it2) {    
            BoardPos* ps = *it2;
            if (ps->type == 9) {
                int res = countArea(ps->i, ps->j, 0);
                if (res > 0) scoreBlack += res;
            }
        }
        scoreBlack += (gr->stones).size();
    }
    for (ItG it = wGroups.begin(); it != wGroups.end(); ++it) {
        StoneGroup* gr = *it;
        for (It it2 = (gr->liberties).begin(); it2 != (gr->liberties).end(); ++it2) {    
            BoardPos* ps = *it2;
            if (ps->type == 9) {
                int res = countArea(ps->i, ps->j, 1);
                if (res > 0) scoreWhite += res; 
            }
        }
        scoreWhite += (gr->stones).size();
    }
    scoreWhite += komi;
}

void GoBoard2::printBoard() {
    std::ofstream flog;
    flog.open("log.txt", std::ios::app);
    flog << std::endl;
    for (int i = 0; i < SIZE; ++i) {
        for (int j = 0; j < SIZE; ++j) {
            if (board[i][j].isBlack2()) flog << 'X';
            else if (board[i][j].isWhite2()) flog << 'O';
            else if (board[i][j].koMark()) flog << ":";
            else flog << ".";
        }
        flog << std::endl;
    }
    flog << "Black Cap: " << capBlack << 
                 "  White Cap: " << capWhite << std::endl;
}

void GoBoard2::printScore() {
    std::ofstream flog;
    flog.open("log.txt", std::ios::app);
    flog << std::endl << "Black score: " << scoreBlack << std::endl
         << "White score: " << scoreWhite << std::endl;
    
}

void GoBoard2::printGroups() {
    std::ofstream flog;
    flog.open("log.txt", std::ios::app);
    flog << "black: " << bGroups.size() << std::endl;
    for (ItG it = bGroups.begin(); it != bGroups.end(); ++it) {
        StoneGroup* gr = *it;
        flog << "Stones: " << (gr->stones).size() << std::endl;
        for (It it2 = (gr->stones).begin(); it2 != (gr->stones).end(); ++it2) {    
            BoardPos* ps = *it2;
            flog << (int)(*ps).i << ',' << (int)(*ps).j << ' ';
        }
        flog << std::endl << "Liberties: " << (gr->liberties).size() << std::endl;
        for (It it2 = (gr->liberties).begin(); it2 != (gr->liberties).end(); ++it2) {    
            BoardPos* ps = *it2;
            flog << (int)(*ps).i << ',' << (int)(*ps).j << ' ';
        }
        flog << std::endl;
    }
    flog << std::endl;
    flog << "white: " << wGroups.size() << std::endl;
    for (ItG it = wGroups.begin(); it != wGroups.end(); ++it) {
        StoneGroup* gr = *it;
        flog << "Stones: " << (gr->stones).size() << std::endl;
        for (It it2 = (gr->stones).begin(); it2 != (gr->stones).end(); ++it2) {    
            BoardPos* ps = *it2;
            flog << (int)(*ps).i << ',' << (int)(*ps).j << ' ';
        }
        flog << std::endl << "Liberties: " << (gr->liberties).size() << std::endl;
        for (It it2 = (gr->liberties).begin(); it2 != (gr->liberties).end(); ++it2) {    
            BoardPos* ps = *it2;
            flog << (int)(*ps).i << ',' << (int)(*ps).j << ' ';
        }
        flog << std::endl;
    }
    flog << std::endl;
}

void GoBoard2::printMoves() {
    std::ofstream flog;
    flog.open("log.txt", std::ios::app);
    flog << "black Moves: " << freeMoves.size() << std::endl;
    for (ItI it = freeMoves.begin(); it != freeMoves.end(); ++it) {
        int p = *it;
        flog << p/SIZE << ',' << p%SIZE << ' ';
    }
    flog << std::endl << std::endl;
}
