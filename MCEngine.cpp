/* 
 * File:   MCEngine.cpp
 * Author: Red
 * 
 * Created on 16 de marzo de 2013, 1:30
 */

#include "MCEngine.h"

MCEngine::MCEngine() {
}

MCEngine::MCEngine(const MCEngine& orig) {
}

MCEngine::~MCEngine() {
}

GoMove MCEngine::nextMove(GoBoard2& board, char color) {
    VecM moves = getAllMoves(board, color);
    if (moves.size() == 0) return GoMove(-1,-1,-1, false);
    int it = 3000;
    GoBoard2* b2;
    while(it--) {
        b2 = new GoBoard2(board);
        char ind = rand()%moves.size();
        moves[ind].tot++;
        //moves[ind].win += b2->randomGame(moves[ind].coord, color);
        delete b2;
    }
    double best = -1;
    char ind = -1;
    for (int i = 0; i < moves.size(); ++i) {
        double temp = (double)moves[i].win/(double)moves[i].tot;
        if (temp > best) {
            best = temp;
            ind = i;
        }
    }
    return GoMove(moves[ind].coord/board.SIZE, moves[ind].coord%board.SIZE, 
                  color, true);
}

VecM MCEngine::getAllMoves(GoBoard2& board, char color) {
    VecM moves(0);
    mcMove ini = {0,0,0};
    for (int i = 0; i < board.SIZE; ++i) {
        for (int j = 0; j < board.SIZE; ++j) {
            if (board.isLegal(i, j, color) and !board.isEye(i, j, color)) {
                ini.coord = i*board.SIZE + j;
                moves.push_back(ini);
            }
        }
    }
    return moves;
}
