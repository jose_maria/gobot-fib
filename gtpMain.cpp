/* 
 * File:   GtpPlay.cpp
 * Author: Red
 * 
 * Created on 12 de marzo de 2013, 21:15
 */

#ifndef GTPMAIN
#define	GTPMAIN

extern "C" {
    #include "gtp.h"
}
#include "GoMove.h"
#include "GoBoard3.h"
#include "MCEngine.h"
#include "MCTSEngine.h"
#include "UCTEngine.h"
#include "MCEngine3.h"
#include "MCTSEngine3.h"
#include "UCTEngine3.h"
#include "RAVEEngine.h"

static int Gtp_protocol_version(char *s);
static int Gtp_name(char *s);
static int Gtp_version(char *s);
static int Gtp_known_command(char *s);
static int Gtp_list_commands(char *s);
static int Gtp_final_status_list(char *s);
static int Gtp_quit(char *s);
static int Gtp_boardsize(char *s);
static int Gtp_clear_board(char *s);
static int Gtp_komi(char *s);
static int Gtp_play(char *s);
static int Gtp_genmove(char *s);
static int Gtp_reg_genmove(char *s);

static struct gtp_command commands[] = {
    {"protocol_version", Gtp_protocol_version},
    {"name", Gtp_name},
    {"version", Gtp_version},
    {"known_command", Gtp_known_command},
    {"final_status_list", Gtp_final_status_list},
    {"list_commands", Gtp_list_commands},
    {"quit", Gtp_quit},
    {"boardsize", Gtp_boardsize},
    {"clear_board", Gtp_clear_board},
    {"komi", Gtp_komi},
    {"play", Gtp_play},
    {"genmove", Gtp_genmove},
    {"reg_genmove", Gtp_reg_genmove},
    {NULL, NULL}
};

static GoBoard3 board;
static RAVEEngine engine;

static int gtpMain() {
    engine = RAVEEngine();
    FILE *inp = stdin;
    FILE *outp = stdout;
    setbuf(outp, NULL);
    gtp_main_loop(commands, inp, outp, NULL);
}

static int Gtp_protocol_version(char *s) {
    return gtp_success("2");
}

static int Gtp_name(char *s) {
    return gtp_success("GoBot UCT-RAVE");
}

static int Gtp_version(char *s) {
    return gtp_success("0.0.2 - When I'm ahead or behind by a lot I may play"
            " nonsensical moves. I will resign if I can understand my loss.");
}

static int Gtp_known_command(char *s) {
    int i = 0;
    while (commands[i].name != NULL and strcmp(s, commands[i].name) != 0) i++;
    if (commands[i].name != NULL) return gtp_success("true");
    return gtp_success("false");
}

static int Gtp_list_commands(char *s) {
    gtp_start_response(GTP_SUCCESS);
    for (int i = 0; commands[i].name != NULL; i++) {
        gtp_printf("%s\n", commands[i].name);
    }
    gtp_printf("\n");
    return GTP_OK;
}

static int Gtp_final_status_list(char *s) {
    if (!engine.hasfinalstate) return gtp_failure("oops");
    gtp_start_response(GTP_SUCCESS);
    bool first = true;
    if (s[0] == 'a') { //alive
        for (int i = 0; i < board.size2; ++i) {
            if (board.b[i] != EMPTY && engine.alive[i] > 0.8) {
                if (!first) gtp_printf(" ");
                else first = false;
                gtp_print_vertex((i-1)/board.size - 1, (i-1)%board.size);
            }
        }
    }
    else if (s[0] == 'd') { //dead
        for (int i = 0; i < board.size2; ++i) {
            if (board.b[i] != EMPTY && engine.dead[i] > 0.8) {
                if (!first) gtp_printf(" ");
                else first = false;
                gtp_print_vertex((i-1)/board.size - 1, (i-1)%board.size);
            }
        }
    }
    free(engine.alive);
    free(engine.dead);
    return gtp_finish_response();
}

static int Gtp_quit(char *s) {
    gtp_success("");
    return GTP_QUIT;
}

static int Gtp_boardsize(char *s) {
    int size;
    sscanf(s, "%d", &size); // we don't check if it's a proper size
    board.setSize(size);
    board.initBoard();
//    board.initBoardSet();
    //board = GoBoard2(size);
    gtp_internal_set_boardsize(size);
    return gtp_success("");
}

static int Gtp_clear_board(char *s) {
    board.setSize(board.size - 2);
    board.initBoard();
    engine.resetGame();
    //board.clearBoard();
    return gtp_success("");
}

static int Gtp_komi(char *s) {
    float komi;
    sscanf(s, "%f", &komi); //we don't check if it's a proper komi
    board.setKomi(komi);
    return gtp_success("");
}

static int Gtp_play(char *s) {
    //we don't check if it's a valid move
    int i, j, color;
    gtp_decode_move(s, &color, &i, &j); // esto igual no esta bien caps sens.
    if (i == -1 && j == -1) {
        engine.playMove(PASS);
        board.playMove(PASS, color);
    }
    else {
        board.playMove((i+1)*board.size + j + 1, color);
        engine.playMove((i+1)*board.size + j + 1);
    }
    //GoMove mov(i, j, color+1, true);
    //board.nextMove(mov);
    return gtp_success("");
}

static int Gtp_genmove(char *s) {
    int color;
    gtp_decode_color(s, &color);
    GoMove move = engine.nextMove(board, color);
    if (!move.valid) return gtp_success("resign");
    if (move.i == -1 && move.j == -1) board.playMove(PASS, color);
    else board.playMove((move.i+1)*board.size + move.j + 1, color);
    //board.nextMove(move);
    gtp_start_response(GTP_SUCCESS);
    gtp_print_vertex(move.i, move.j);
    return gtp_finish_response();
}

static int Gtp_reg_genmove(char *s) {
    int color;
    gtp_decode_color(s, &color);
    engine.resetGame();
    GoMove move = engine.nextMove(board, color);
    if (!move.valid) return gtp_success("resign");
    gtp_start_response(GTP_SUCCESS);
    gtp_print_vertex(move.i, move.j);
    return gtp_finish_response();
}

#endif
