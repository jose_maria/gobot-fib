/* 
 * File:   GoBoard3.cpp
 * Author: Red
 * 
 * Created on 7 de abril de 2013, 20:25
 */

#include "GoBoard3.h"

GoBoard3::GoBoard3() {
}

GoBoard3::GoBoard3(const GoBoard3& orig) {
    size = orig.size;
    size2 = orig.size2;
    komi = orig.komi;
    ko = orig.ko;
    koColor = orig.koColor;
    lastMove = orig.lastMove;
    lastMove2 = orig.lastMove2;
    turns = orig.turns;
    flen = orig.flen;
    hashact = orig.hashact;
    hashlen = orig.hashlen;
    superko = orig.superko;
    capBlack = orig.capBlack;
    capWhite = orig.capWhite;
    caplen = orig.caplen;
    atalen = orig.atalen;
    allocBoard();
    memcpy(b, orig.b, memsize);
}

GoBoard3::~GoBoard3() {
    if (b) free(b);
}

void GoBoard3::resetStateBoard(const GoBoard3& orig) {
    size = orig.size;
    size2 = orig.size2;
    komi = orig.komi;
    ko = orig.ko;
    koColor = orig.koColor;
    lastMove = orig.lastMove;
    lastMove2 = orig.lastMove2;
    turns = orig.turns;
    flen = orig.flen;
    capBlack = orig.capBlack;
    capWhite = orig.capWhite;
    caplen = orig.caplen;
    atalen = orig.atalen;
    hashact = orig.hashact;
    hashlen = orig.hashlen;
    superko = orig.superko;
    memcpy(b, orig.b, memsize);
}

void GoBoard3::allocBoard() {
    int bsize = size2 * sizeof(*b);
    int gsize = size2 * sizeof(*g);
    int psize = size2 * sizeof(*p);
    int fsize = size2 * sizeof(*f);
    int ginfosize = size2 * sizeof(*ginfo);
    int nscolorsize = size2 * sizeof(*nscolor);
    int zobsize = size2 * 3 * sizeof(*zob);
    int hashsize = (size2*4) * sizeof(*hashes);
    int movsize = MAXMOVES * sizeof(*movs);
    int capsize = size2 * sizeof(*capturable);
    int patternsize = size2 * sizeof(*patternCache);
    memsize = bsize + gsize + psize + fsize + ginfosize + nscolorsize +
              zobsize + hashsize+ movsize + capsize*2 + patternsize;
    char *x = (char*)malloc(memsize);
    b = (int*)x; x += bsize;
    g = (int*)x; x += gsize;
    p = (int*)x; x += psize;
    f = (int*)x; x += fsize;
    ginfo = (struct groupInfo*) x; x += ginfosize;
    nscolor = (struct neighborsColor*) x; x += nscolorsize;
    zob = (int*)x; x += zobsize;
    hashes = (int*)x; x += hashsize;
    movs = (int*)x; x += movsize;
    capturable = (int*)x; x += capsize;
    atariable = (int*)x; x += capsize;
    patternCache = (char*)x; x += patternsize;
}

void GoBoard3::setSize(int s) {
    size = s+2;
    size2 = size*size;
    allocBoard();
    std::memset(b, 0, memsize);
}

void GoBoard3::initBoard() {
    komi = 0;
    ko = PASS;
    koColor = EMPTY;
    lastMove = RESIGN;
    lastMove2 = RESIGN;
    turns = 0;
    hashlen = 0;
    caplen = 0;
    atalen = 0;
    
    superko = false;
    for (int i = 0; i < size; ++i) b[i] = b[size2 - size + i] = BORDER;
    for (int i = 0; i < size2; i += size) b[i] = b[i + size - 1] = BORDER;
    for_each_point {
        if (!isBorder(cp)) {
            for_each_neighbor(c, cp, {
                nscolor[cp].col[b[c]]++;
            });
        }
    }
    flen = 0;
    for (int i = size; i < size2-size; ++i) {
        if (i%size != 0 && (i+1)%size != 0) f[flen++] = i;
    }
    for (int i = 0; i < size2 * 3; ++i) zob[i] = rand();
    memset(patternCache, -1, sizeof(*patternCache));
}

void GoBoard3::remCapturable(int group) {
    for (int i = 0; i < caplen; ++i) {
        if (unlikely(capturable[i] == group)) {
            capturable[i] = capturable[--caplen];
            return;
        }
    }
}

void GoBoard3::remAtariable(int group) {
    for (int i = 0; i < atalen; ++i) {
        if (unlikely(atariable[i] == group)) {
            atariable[i] = atariable[--atalen];
            return;
        }
    }
}

inline void GoBoard3::addLibToGroup(int coord, int group) {
    //printf("addLibToGroup\n");
    if (ginfo[group].libs < LIBS_MAX) {
        for (int i = 0; i < ginfo[group].libs; ++i) {
            if (unlikely(ginfo[group].coords[i] == coord)) return;
        }
        ginfo[group].coords[ginfo[group].libs++] = coord;
        int libs = ginfo[group].libs;
        if (libs == 1) capturable[caplen++] = group;
        else if (libs == 2) {
            remCapturable(group);
            atariable[atalen++] = group;
        }
        else if (libs == 3) remAtariable(group);
    }
}

inline void GoBoard3::searchMoreLibs(int coord, int group) {
    //printf("SearchMoreLibs %i,%i\n", coord/size, coord%size);
    //printGroups();
    //printBoard();
    unsigned char used[size2];
    memset(used, 0, sizeof(used));
    for (int i = 0; i < ginfo[group].libs; ++i) {
        used[ginfo[group].coords[i]] = 1;
    }
    
    used[coord] = 1;
    for_each_in_group(group, {
        for_each_neighbor(c2, c, {
            if (b[c2] == EMPTY && !used[c2]) {
                ginfo[group].coords[ginfo[group].libs++] = c2;
                used[c2] = 1;
                if (unlikely(ginfo[group].libs == LIBS_MAX)) {
                    //printGroups();
                    return;
                }
            }
        });
    });
    //printGroups();
}

inline void GoBoard3::remLibFromGroup(int coord, int group) {
    //printf("remLibFromGroup\n");
    for (int i = 0; i < ginfo[group].libs; ++i) {
        if (unlikely(ginfo[group].coords[i] == coord)) {
            ginfo[group].coords[i] = ginfo[group].coords[--ginfo[group].libs];
            ginfo[group].coords[ginfo[group].libs] = 0;
            if (ginfo[group].libs == LIBS_MAX-1) searchMoreLibs(coord, group);
            int libs = ginfo[group].libs;
            if (libs == 0) remCapturable(group);
            else if (libs == 1) {
                capturable[caplen++] = group;
                remAtariable(group);
            }
            else if (libs == 2) atariable[atalen++] = group;
            return;
        }
    }
}

int GoBoard3::createGroup(int coord) {
    //printf("createGroup\n");
    int group = coord;
    for_each_neighbor(c, coord, {
        if (isEmpty(c)) {
            ginfo[group].coords[ginfo[group].libs++] = c;
        }
    });
    int libs = ginfo[group].libs;
    if (libs == 2) atariable[atalen++] = group;
    else if (libs == 1) capturable[caplen++] = group;
    g[coord] = group;
    p[coord] = 0;
    
    return group;
}

void GoBoard3::addToGroup(int orig_c, int coord, int group) {
    //printf("addToGroup\n");
    g[orig_c] = group;
    p[orig_c] = p[group];
    p[group] = orig_c;
    for_each_neighbor(c, orig_c, {
        if (isEmpty(c)) {
            addLibToGroup(c, group);
        }
    });
}

void GoBoard3::fusionGroups(int group1, int group2) { //from group2 to group1
    //printf("fusionGroups\n");
    
    int libs = ginfo[group2].libs;
    if (libs == 1) remCapturable(group2);
    else if (libs == 2) remAtariable(group2);
    
    int last;
    for_each_in_group(group2, {
        last = c;
        g[c] = group1;
    });
    p[last] = p[group1];
    p[group1] = group2;
    
    int origlibs = ginfo[group1].libs;
    for (int i = 0; i < ginfo[group2].libs &&
                    ginfo[group1].libs < LIBS_MAX; ++i) {
        int lib = ginfo[group2].coords[i];
        int j = 0;
        while(j < origlibs && ginfo[group1].coords[j] != lib) ++j;
        if (j == origlibs) {
            ginfo[group1].coords[ginfo[group1].libs++] = lib;
            libs = ginfo[group1].libs;
            if (libs == 1) capturable[caplen++] = group1;
            else if (libs == 2) {
                remCapturable(group1);
                atariable[atalen++] = group1;
            }
            else if (libs == 3) remAtariable(group1);
        }
    }
    //libs = ginfo
    memset(&ginfo[group2], 0, sizeof(struct groupInfo));
}

void GoBoard3::captureStone(int coord, int group) {
    for_each_neighbor(c, coord, {
        nscolor[c].col[b[coord]]--;
        nscolor[c].col[EMPTY]++;
        if (g[c] && g[c] != group) addLibToGroup(coord, g[c]);
        patternCache[c] = -1;
    });
    for_each_diagonal(c2, coord, {
        patternCache[c] = -1;
    });
    hashact ^= zob[coord*3 + b[coord]];
    hashact ^= zob[coord*3];
    b[coord] = 0;
    g[coord] = 0;
    f[flen++] = coord;
}

int GoBoard3::captureGroup(int group) {
    //printf("captureGroup\n");
    int cont = 0;
    char black = isBlack(group);
    for_each_in_group(group, {
        captureStone(c, group);
        cont++;
    });
    if (black) capBlack += cont;
    else capWhite += cont;
    memset(&ginfo[group], 0, sizeof(struct groupInfo));
    return cont;
}

int GoBoard3::updateNeighbor(int orig_c, int coord, int color, 
                               int group) {
    nscolor[coord].col[color]++;
    nscolor[coord].col[EMPTY]--;
    
    int new_color = b[coord];
    int new_group = g[coord];
    if (!new_group) return group;
    int opcolor = oppositeColor[color];
    
    if (new_color == WHITE || new_color == BLACK) {
        remLibFromGroup(orig_c, new_group);
    }
    if (color == new_color && new_group != group) {
        if (!group) {
            group = new_group;
            addToGroup(orig_c, coord, group);
        }
        else {
            fusionGroups(group, new_group);
        }
    }
    else if (new_color == opcolor) {
        if (unlikely(isCaptured(new_group))) captureGroup(new_group);
    }
    return group;
}

int GoBoard3::playInCoord(int coord, int color, int fcoord) {
    //fprintf(stderr, "playCoord\n");
    
    int group = 0;
    
    for_each_neighbor(c, coord, {
        group = updateNeighbor(coord, c, color, group);
        patternCache[c] = -1;
    });
    
    for_each_diagonal(c2, coord, {
        patternCache[c] = -1;
    });
    
    if (unlikely(!group)) group = createGroup(coord);
    
    patternCache[coord] = -1;
    b[coord] = color;
    if (isCaptured(group)) captureGroup(group);
    f[fcoord] = f[--flen];
    hashact ^= zob[coord*3];
    hashact ^= zob[coord*3 + color];
    superko = repeatHash();
    hashes[hashlen++] = hashact;
    lastMove2 = lastMove;
    lastMove = coord;
    ko = PASS;
    koColor = EMPTY;
    movs[turns++] = coord;
    return 1;
}

int GoBoard3::playInEye(int coord, int color, int fcoord) {
    //fprintf(stderr, "playEye\n");
    int captured = 0;
    int capcoord = PASS;
    for_each_neighbor(c2, coord, {
        nscolor[c2].col[color]++;
        nscolor[c2].col[EMPTY]--;
        int group = g[c2];
        if (group) {
            remLibFromGroup(coord, group);
            if (isCaptured(group)) {
                capcoord = c2;
                captured += captureGroup(group);
            }
        }
        patternCache[c2] = -1;
    });
    for_each_diagonal(c3, coord, {
        patternCache[c3] = -1;
    });
    if (captured == 1) { // ko
        ko = capcoord;
        koColor = oppositeColor[color];
    }
    else {
        ko = PASS;
        koColor = EMPTY;
    }
    createGroup(coord);
    lastMove2 = lastMove;
    lastMove = coord;
    patternCache[coord] = -1;
    b[coord] = color;
    f[fcoord] = f[--flen];
    hashact ^= zob[coord*3];
    hashact ^= zob[coord*3 + color];
    superko = repeatHash();
    hashes[hashlen++] = hashact;
    movs[turns++] = coord;
    return 1;
}

int GoBoard3::playMove(int coord, int color) {
    if (hashlen > 0) hashact = hashes[hashlen-1];
    if (unlikely(isPass(coord))) {
        lastMove2 = lastMove;
        lastMove = PASS;
        ko = PASS;
        koColor = EMPTY;
        movs[turns++] = PASS;
        return 0;
    }
    
    for_each_free_point {
        if (f[c] == coord) {
            if (unlikely(isSurrounded(coord, oppositeColor[color]))) {
                return playInEye(coord, color, c);
            }
            else {
                return playInCoord(coord, color, c);
            }
        }
    }
    
}

float GoBoard3::getScore(double dynkomi) {
    int scoreBlack = 0;
    int scoreWhite = 0;
    for_each_point {
        if (isEmpty(cp)) {
            if (isSurrounded(cp, BLACK)) scoreBlack++;
            else if (isSurrounded(cp, WHITE)) scoreWhite++;
        }
        else if (isBlack(cp)) scoreBlack++;
        else if (isWhite(cp)) scoreWhite++;
    }
    return komi + scoreWhite - scoreBlack + dynkomi;
}

void GoBoard3::setKomi(float k) {
    komi = k;
}

inline bool GoBoard3::isFalseEye(int coord, int color) {
    char count[4] = { 0, 0, 0, 0};
    for_each_diagonal(c, coord, {
        count[b[c]]++;
    });
    if (count[BORDER] > 0) return !!count[oppositeColor[color]];
    return count[oppositeColor[color]] >= 2;
}

inline bool GoBoard3::isLegalMove(int coord, int color) {
    if (unlikely(!isEmpty(coord))) return false;
    if (unlikely(ko == coord && koColor == color)) return false;
    if (likely(!isSurrounded(coord, oppositeColor[color]))) return true;
    int captured = 0;
    for_each_neighbor(c, coord, {
        if (unlikely(ginfo[g[c]].libs == 1)) ++captured;
    });
    return !!captured;
}

inline bool GoBoard3::isSuicide(int coord, int color) {
    if (nscolor[coord].col[EMPTY] > 0) return false;
    int lib = -1;
    for_each_neighbor(c, coord, {
        if (b[c] == color) {
            if (likely(ginfo[g[c]].libs > 1)) return false;
            int act = ginfo[g[c]].coords[0];
            if (lib == -1) lib = act;
            else if (lib != act) return false;
        }
        else if (b[c] == oppositeColor[color]) {
            if (unlikely(ginfo[g[c]].libs == 1)) return false;
        }
    });
    return true;
}

inline bool GoBoard3::isSelfAtari(int coord, int color) {
    int empty = -1;
    if (nscolor[coord].col[EMPTY] > 1) return false;
    else if (nscolor[coord].col[EMPTY] == 1) {
        for_each_neighbor(c1, coord, {
            if (isEmpty(c1)) empty = c1; 
        });
    }
    int lib1 = empty;
    bool checksnapback = false;
    for_each_neighbor(c, coord, {
        if (b[c] == color) {
            if (likely(ginfo[g[c]].libs > 2)) return false;
            if (unlikely(ginfo[g[c]].libs == 2)) {
                int act = ginfo[g[c]].coords[0];
                if (act == coord) act = ginfo[g[c]].coords[1];
                if (lib1 != -1) {
                    if (lib1 != act) return false;
                }
                else lib1 = act;
            }
        }
        else if (b[c] == oppositeColor[color]) {
            if (ginfo[g[c]].libs == 1) return false;
            else if (ginfo[g[c]].libs == 2) checksnapback = true;
        }
    });
    
    if (!checksnapback || empty == -1 || nscolor[empty].col[EMPTY] > 1) {
        return true;
    }
    
    for_each_neighbor(c2, empty, {
        if (unlikely(b[c2] == oppositeColor[color] &&
           (ginfo[g[c2]].libs > 2 ||
           (ginfo[g[c2]].libs == 2 && ginfo[g[c2]].coords[0] != coord &&
            ginfo[g[c2]].coords[1] != coord)))) {
            return true;
        }
        if (b[c2] == color && ginfo[g[c2]].libs == 1) return true;
    });
    return false;
}

bool GoBoard3::isLadderp(int coord, int group, int color) {
    return isLadder(coord, group, color);
}

inline bool GoBoard3::isLadder(int coord, int group, int color) {
    if (ladderCache[coord] < 2) return ladderCache[coord];
    //fprintf(stderr, "Start %i,%i %i,%i\n", coord/size, coord%size,
    //                                     group/size, group%size);
    if (nscolor[coord].col[BORDER] == 1 && nscolor[coord].col[color] == 1 &&
        nscolor[coord].col[EMPTY] == 2) {
        if ((isBorder(coord-size) && b[coord+size] == color) || 
            (isBorder(coord+size) && b[coord-size] == color) ||
	    (isBorder(coord-1) && b[coord+1] == color) ||
	    (isBorder(coord+1) && b[coord-1] == color)) {
                bool isladder = isBorderLadder(coord, color);
                ladderCache[coord] = isladder;
		return isladder;
	}
    }
    bool isladder = isExpensiveLadder(coord, group, color);
    //fprintf(stderr, "End\n");
    ladderCache[coord] = isladder;
    return isladder;
}


bool GoBoard3::isBorderLadder(int coord, int color) {
    //fprintf(stderr, "TestBorder\n");
    // | ? ?   | c1  ?
    // | . O   | .  g1
    // | c X   | c  X
    // | . O   | .  g2
    // | ? ?   | c2  ?

    int c1, c2, g1, g2;

    if (isBorder(coord-size) || isBorder(coord+size)) {
        c1 = coord - 2;
	c2 = coord + 2;
	if (isBorder(coord-size)) {
	    g1 = c1 + 1 + size;
	    g2 = c2 - 1 + size;
        }
	else {	
	    g1 = c1 + 1 - size;
	    g2 = c2 - 1 - size;
	}
    }
    else {
        c1 = coord - size - size;
        c2 = coord + size + size;
        if (isBorder(coord-1)) {
            g1 = c1 + size + 1;
            g2 = c2 - size + 1;
        }
        else {
            g1 = c1 + size - 1;
            g2 = c2 - size - 1;
        }
    }
    //should we consider if c1 and c2 are in atari?
    bool at1 = ginfo[g[c1]].libs == 1;
    bool at2 = ginfo[g[c2]].libs == 1;
    if (b[c1] == color && b[c2] == color && !at1 && !at2) return false;
    int lib1 = ginfo[g[g1]].libs;
    int lib2 = ginfo[g[g2]].libs;
    if ((lib1 < 2 && !at1) || (lib2 < 2 && !at2)) return false;
    if (b[c1] == color && !at1 && lib1 < 3) return false;
    if (b[c2] == color && !at2 && lib2 < 3) return false;
    return true;
}


bool GoBoard3::isExpensiveLadder(int coord, int group, int color) {
    //fprintf(stderr, "TestExpensive\n");
    int libs = nscolor[coord].col[EMPTY];
    for_each_neighbor(clib, coord, {
        if (b[clib] == color) {
            libs += ginfo[g[clib]].libs - 1;
        }
    });
    if (libs != 2) return false;
    //fprintf(stderr, "Enter\n");
    //GoBoard3 * boardset;
    //boardset = (GoBoard3*)malloc(11*11*2 * sizeof(GoBoard3));
    int used[size2];
    memset(used, 0, sizeof(used));
    for_each_in_group(group, {
        for_each_neighbor(cc, c, {
            if (b[cc] == oppositeColor[color] && !used[g[cc]] && 
                ginfo[g[cc]].libs == 1) {
                int libop = ginfo[g[cc]].coords[0];
                GoBoard3 b2(*this);
                bool isladder = isRecursiveLadder(b2, group,
                                                  libop, color);
                if (!isladder) {
                    //free(boardset);
                    return false;
                }
                used[g[cc]] = 1;
            }
        });
    });
    GoBoard3 b2(*this);
    bool isladder = isRecursiveLadder(b2, group, ginfo[g[group]].coords[0], color);
    //free(boardset);
    //return false;
    return isladder;
}


bool GoBoard3::isRecursiveLadder(GoBoard3& b2,
                                   int group, int next, int color) {
    //fprintf(stderr, "Recursive: %i,%i, %i,%i\n", next/size, next%size,
    //                                              group/size, group%size);
    //b2.printBoard();
    b2.playMove(next, color);
    //fprintf(stderr, "  played\n");
    //b2.printBoard();
    group = b2.g[group];
    //fprintf(stderr, "  group: %i,%i", group/size, group%size);
    if (b2.ginfo[group].libs == 1) return true;
    if (b2.ginfo[group].libs > 2) return false;
    //fprintf(stderr, "  Not Trivial\n");
    //b2.printBoard();
    for_each_neighbor(cc, next, {
        //fprintf(stderr, "    cc: %i,%i\n", cc/size, cc%size);
    	if (b2.b[cc] == oppositeColor[color] && b2.ginfo[b2.g[cc]].libs == 1) {
            return false;
        }
    });
    //fprintf(stderr, "  Not Capture\n");
    bool isladder = false;
    int count = 0;
    int libs[2];
    for (int i = 0; i < 2 && !isladder; ++i) {
        int opponentmove = b2.ginfo[b2.g[group]].coords[i];
        int ourmove = b2.ginfo[b2.g[group]].coords[1-i];
	
        int lib = b2.nscolor[ourmove].col[EMPTY];
        /*for_each_neighbor(clib, ourmove, {
            if (b2.b[clib] == color) {
                lib += b2.ginfo[b2.g[clib]].libs - 1;
            }
            if (clib == opponentmove) lib--;
        });*/
        //fprintf(stderr, "    lib: %i\n", lib);
        if (lib <= 2) libs[count++] = i;
    }
    //fprintf(stderr, "  Count: %i\n", count);
    for (int i = 0; i < count && !isladder; ++i) {
        int opponentmove = b2.ginfo[b2.g[group]].coords[libs[i]];
	//fprintf(stderr, "    Opponent: %i,%i\n", opponentmove/size,
        //                                          opponentmove%size);
        if (i != count - 1) {
            //b3 = boardset++;
            GoBoard3 b3(b2);
            b3.playMove(opponentmove, oppositeColor[color]);
            if (b3.ginfo[b3.g[opponentmove]].libs > 1) {
                isladder = isRecursiveLadder(b3, group,
                                      b3.ginfo[b3.g[group]].coords[0], color);
            }
            //free(b3);
            //b3->resetStateBoard(b2);
        }
        else {
            b2.playMove(opponentmove, oppositeColor[color]);
            if (b2.ginfo[b2.g[opponentmove]].libs > 1) {
                isladder = isRecursiveLadder(b2, group,
                                      b2.ginfo[b2.g[group]].coords[0], color);
            }
        }
        //fprintf(stderr, "    resetBoard\n");
        //fprintf(stderr, "    playBoard\n");
    }
    return isladder;
}

inline bool GoBoard3::canBeLadder(int group, int coord, int opponent,
                                   int color) {
    bool neigh = false;
    for_each_8neighbor(cn, coord, {
        if (cn == opponent) neigh = true;
    });
    if (!neigh || nscolor[opponent].col[color] != 1 || 
        nscolor[coord].col[EMPTY] != 2) return false;
    
    GoBoard3 b2(*this);
    b2.playMove(opponent, oppositeColor[color]);
    group = b2.g[group];
    return isRecursiveLadder(b2, group, b2.ginfo[group].coords[0], color);
}

int GoBoard3::countEscape(int coord, int color) {
    int libs = 0;
    for_each_neighbor(c, coord, {
        if (b[c] == color) libs += ginfo[g[c]].libs;
    });
    return libs + nscolor[c].col[EMPTY];
}

int GoBoard3::playRandomMove(int color) {
    //fprintf(stderr, "playRandom\n");
    if (unlikely(flen == 0)) {
        playMove(PASS, color);
        return PASS;
    }
    int ind = 0;
    bool allowatari = (rand()%1000 < P_SELFATARI);
    int start = rand()%flen;
    for (int i = start; i < flen; ++i) {
        int coord = f[i];
        if ((!isSurrounded(coord, color) || isFalseEye(coord, color)) &&
            isLegalMove(coord, color) && !isSuicide(coord, color)) {
            if (allowatari || !isSelfAtari(coord, color)) {
                playMove(coord, color);
                return coord;
            }
            else if (isSelfAtari(coord, color)) moves[ind++] = coord;
        }
    }
    for (int i = 0; i < start; ++i) {
        int coord = f[i];
        if ((!isSurrounded(coord, color) || isFalseEye(coord, color)) &&
            isLegalMove(coord, color) && !isSuicide(coord, color)) {
            if (allowatari || !isSelfAtari(coord, color)) {
                playMove(coord, color);
                return coord;
            }
            else if (isSelfAtari(coord, color)) moves[ind++] = coord;
        }        
    }
    if (ind) {
        //fprintf(stderr, "selfatari\n");
        int coord = moves[rand()%ind];
        playMove(coord, color);
        return coord;
    }
    playMove(PASS, color);
    return PASS;
}

int GoBoard3::playHeuristicMove(int color) {
    //fprintf(stderr, "playHeuristic\n");
    memset(ladderCache, 2, sizeof(ladderCache));
    memset(patternCache, -1, sizeof(patternCache));
    int opColor = oppositeColor[color];
    int prob;
    int ind = 0;
    bool neighboratari = false;
    if (likely(!isPass(lastMove) && !isResign(lastMove))) {
        prob = rand()%1000;
        if (prob < P_1LIBS) {
            if (unlikely(ginfo[g[lastMove]].libs == 1)) { //try to capture
                int coord = ginfo[g[lastMove]].coords[0];
                if (likely(ko != coord || color != koColor)) {
                    //fprintf(stderr, "kill1\n");
                    playMove(coord, color);
                    return coord;
                }
            }
            for_each_neighbor(c2, lastMove, { //try to save
                if (unlikely(b[c2] == color && ginfo[g[c2]].libs == 1)) {
                    neighboratari = true;
                    int ret = ginfo[g[c2]].coords[0];
                    if (!isSuicide(ret, color) && !isSelfAtari(ret, color) &&
                        (ko != ret || color != koColor) &&
                        !isLadder(ret, g[c2], color)) {
                        moves[ind++] = ret;
                    }
                    
                    for_each_in_group(g[c2], { //try to save by capturing
                        for_each_neighbor(c3, c, {
                            if (unlikely(b[c3] == oppositeColor[color] &&
                                ginfo[g[c3]].libs == 1)) {
                                int ret = ginfo[g[c3]].coords[0];
                                if (likely(ko != ret || color != koColor)) {
                                    int i = 0;
                                    while (i < ind && moves[i] != ret) i++;
                                    if (i == ind) moves[ind++] = ret;
                                }
                            }
                        });
                    });
                }
            });
            if (ind) {
                //fprintf(stderr, "save1\n");
                int coord = moves[rand()%ind];
                playMove(coord, color);
                return coord;
            }
        }
        prob = rand()%1000;
        if (prob < P_2LIBS) {
            if (unlikely(ginfo[g[lastMove]].libs == 2) && !neighboratari) { 
                int coord = ginfo[g[lastMove]].coords[0]; //try to capture
                int lib1 = 0;
                int lib2 = 0;
                if (!isSuicide(coord, color) &&
                    (ko != coord || color != koColor) &&
                    !isSelfAtari(coord, color)) {
                    //fprintf(stderr, "kill2\n");
                    if (canBeLadder(g[lastMove], ginfo[g[lastMove]].coords[1],
                        coord, oppositeColor[color])) {
                        prob = rand()%1000;
                        if (prob < P_CAPLADDER) {
                            playMove(coord, color);
                            return coord;
                        }
                    }
                    moves[ind++] = coord;
                    lib1 = countEscape(coord, oppositeColor[color]);
                    
                }
                coord = ginfo[g[lastMove]].coords[1];
                if (!isSuicide(coord, color) &&
                    (ko != coord || color != koColor) &&
                    !isSelfAtari(coord, color)) {
                    //fprintf(stderr, "kill2\n");
                    if (canBeLadder(g[lastMove], ginfo[g[lastMove]].coords[0],
                        coord, oppositeColor[color])) {
                        prob = rand()%1000;
                        if (prob < P_CAPLADDER) {
                            playMove(coord, color);
                            return coord;
                        }
                    }
                    moves[ind++] = coord;
                    lib2 = countEscape(coord, oppositeColor[color]);
                }
                if (lib1 && lib2) {
                    if (lib1 < lib2) moves[ind++] = moves[0];
                    else if (lib1 > lib2) moves[ind++] = moves[1];
                }
            }
            for_each_neighbor(c2, lastMove, { //try to save
                int actgroup = 0;
                if (unlikely(b[c2] == color && ginfo[g[c2]].libs == 2)) {
                    int ret = ginfo[g[c2]].coords[0];
                    int lib1 = 0;
                    int lib2 = 0;
                    int coord1;
                    int coord2;
                    if (!isSuicide(ret, color) &&
                        (ko != ret || color != koColor) &&
                        (!isSurrounded(ret, color) || isFalseEye(ret, color)) &&
                        !isSelfAtari(ret, color)) {
                        moves[ind++] = ret;
                        coord1 = ret;
                        lib1 = countEscape(ret, color);
                    }
                    ret = ginfo[g[c2]].coords[1];
                    if (!isSuicide(ret, color) &&
                        (ko != ret || color != koColor) &&
                        (!isSurrounded(ret, color) || isFalseEye(ret, color)) &&
                        !isSelfAtari(ret, color)) {
                        moves[ind++] = ret;
                        coord2 = ret;
                        lib2 = countEscape(ret, color);
                    }
                    if (lib1 && lib2) {
                        if (lib1 > lib2) moves[ind++] = coord1;
                        else if (lib1 < lib2) moves[ind++] = coord2;
                    }
                    for_each_in_group(g[c2], { //try to save by capturing
                        for_each_neighbor(c3, c, {
                            if (unlikely(b[c3] == oppositeColor[color] &&
                                ginfo[g[c3]].libs < 3)) {
                                int lib1 = 0;
                                int lib2 = 0;
                                int coord1;
                                int coord2;
                                int ret = ginfo[g[c3]].coords[0];
                                if (!isSuicide(ret, color) &&
                                    (ko != ret || color != koColor) &&
                                    !isSelfAtari(ret, color)) {
                                    int i = 0;
                                    while (i < ind && moves[i] != ret) i++;
                                    if (i == ind) {
                                        if (ginfo[g[c3]].libs == 2 &&
                                            canBeLadder(g[c3],
                                            ginfo[g[c3]].coords[1], ret,
                                            oppositeColor[color])) {
                                            playMove(ret, color);
                                            return ret;
                                        }
                                        moves[ind++] = ret;
                                        coord1 = ret;
                                        lib1 = countEscape(ret, oppositeColor[color]);
                                    }
                                }
                                if (ginfo[g[c3]].libs == 2) {
                                    ret = ginfo[g[c3]].coords[1];
                                    if (!isSuicide(ret, color) &&
                                        (ko != ret || color != koColor) &&
                                        !isSelfAtari(ret, color)) {
                                        int i = 0;
                                        while (i < ind && moves[i] != ret) i++;
                                        if (i == ind) {
                                            if (canBeLadder(g[c3],
                                                ginfo[g[c3]].coords[0], ret,
                                                oppositeColor[color])) {
                                                playMove(ret, color);
                                                return ret;
                                            }
                                            moves[ind++] = ret;
                                            coord2 = ret;
                                            lib2 = countEscape(ret, oppositeColor[color]);
                                        }
                                    }
                                }
                                if (lib1 && lib2) {
                                    if (lib1 < lib2) moves[ind++] = coord1;
                                    else if (lib1 > lib2) moves[ind++] = coord2;
                                }
                            }
                        });
                    });
                }
            });
            if (ind) {
                //fprintf(stderr, "save2\n");
                int coord = moves[rand()%ind];
                playMove(coord, color);
                return coord;
            }
        }
        prob = rand()%1000;
        if (prob < P_3LIBS) {
            if (unlikely(ginfo[g[lastMove]].libs == 3)  && !neighboratari) { //try to capture
                int coord = ginfo[g[lastMove]].coords[0];
                if (!isSuicide(coord, color) &&
                    (ko != coord || color != koColor) &&
                    !isSelfAtari(coord, color)) {
                    //fprintf(stderr, "kill3\n");
                    moves[ind++] = coord;
                }
                coord = ginfo[g[lastMove]].coords[1];
                if (!isSuicide(coord, color) &&
                    (ko != coord || color != koColor) &&
                    !isSelfAtari(coord, color)) {
                    //fprintf(stderr, "kill3\n");
                    moves[ind++] = coord;
                }
                coord = ginfo[g[lastMove]].coords[2];
                if (!isSuicide(coord, color) &&
                    (ko != coord || color != koColor) &&
                    !isSelfAtari(coord, color)) {
                    //fprintf(stderr, "kill3\n");
                    moves[ind++] = coord;
                }
                
            }
            for_each_neighbor(c2, lastMove, { //try to save
                if (unlikely(b[c2] == color && ginfo[g[c2]].libs == 3)) {
                    int ret = ginfo[g[c2]].coords[0];
                    if (!isSuicide(ret, color) &&
                        (ko != ret || color != koColor) &&
                        (!isSurrounded(ret, color) || isFalseEye(ret, color)) &&
                        !isSelfAtari(ret, color) &&
                        nscolor[ret].col[EMPTY] > 1) {
                        moves[ind++] = ret;
                    }
                    ret = ginfo[g[c2]].coords[1];
                    if (!isSuicide(ret, color) &&
                        (ko != ret || color != koColor) &&
                        (!isSurrounded(ret, color) || isFalseEye(ret, color)) &&
                        !isSelfAtari(ret, color) &&
                        nscolor[ret].col[EMPTY] > 1) {
                        moves[ind++] = ret;
                    }
                    ret = ginfo[g[c2]].coords[2];
                    if (!isSuicide(ret, color) &&
                        (ko != ret || color != koColor) &&
                        (!isSurrounded(ret, color) || isFalseEye(ret, color)) &&
                        !isSelfAtari(ret, color) &&
                        nscolor[ret].col[EMPTY] > 1) {
                        moves[ind++] = ret;
                    }
                    for_each_in_group(g[c2], { //try to save by capturing
                        for_each_neighbor(c3, c, {
                            if (unlikely(b[c3] == oppositeColor[color] &&
                                ginfo[g[c3]].libs < 4)) {
                                int ret = ginfo[g[c3]].coords[0];
                                if (!isSuicide(ret, color) &&
                                    (ko != ret || color != koColor) &&
                                    !isSelfAtari(ret, color)) {
                                    int i = 0;
                                    while (i < ind && moves[i] != ret) i++;
                                    if (i == ind) moves[ind++] = ret;
                                }
                                if (ginfo[g[c3]].libs == 2) {
                                    ret = ginfo[g[c3]].coords[1];
                                    if (!isSuicide(ret, color) &&
                                        (ko != ret || color != koColor) &&
                                        !isSelfAtari(ret, color)) {
                                        int i = 0;
                                        while (i < ind && moves[i] != ret) i++;
                                        if (i == ind) moves[ind++] = ret;
                                    }
                                }
                                if (ginfo[g[c3]].libs == 3) {
                                    ret = ginfo[g[c3]].coords[2];
                                    if (!isSuicide(ret, color) &&
                                        (ko != ret || color != koColor) &&
                                        !isSelfAtari(ret, color)) {
                                        int i = 0;
                                        while (i < ind && moves[i] != ret) i++;
                                        if (i == ind) moves[ind++] = ret;
                                    }
                                }
                            }
                        });
                    });
                }
            });
            if (ind) {
                //fprintf(stderr, "save3\n");
                int coord = moves[rand()%ind];
                playMove(coord, color);
                return coord;
            }
        }
        prob = rand()%1000;
        if (prob < P_PATTERNS) {
            for_each_8neighbor(c, lastMove, {
                if (unlikely(isEmpty(c) && !isSuicide(c, color) && 
                    (ko != c || color != koColor) &&
                    !isSelfAtari(c, color) &&
                    matchSomePattern(c, color))) {
                    //fprintf(stderr, "%i,%i\n", c/size, c%size);
                    int atarin = 0;
                    for_each_neighbor(ca, c, {
                        if (b[ca] == color && ginfo[g[ca]].libs == 1) {
                            atarin = g[ca];
                        }
                    });
                    if (atarin && !isLadder(c, atarin, color)) moves[ind++] = c;
                }
            });
            if (likely(!isPass(lastMove2) && !isResign(lastMove2))) {
                for_each_8neighbor(c2, lastMove2, {
                    if (unlikely(isEmpty(c2) && !isSuicide(c2, color) && 
                        (ko != c2 || color != koColor) &&
                        !isSelfAtari(c2, color) &&
                        matchSomePattern(c2, color))) {
                        //fprintf(stderr, "%i,%i\n", c2/size, c2%size);
                        int atarin = 0;
                        for_each_neighbor(ca, c2, {
                            if (b[ca] == color && ginfo[g[ca]].libs == 1) {
                                atarin = g[ca];
                            }
                        });
                        if (atarin && !isLadder(c2, atarin, color)) {
                            moves[ind++] = c2;
                        }
                    }
                });
            }
            if (ind) {
                //fprintf(stderr, "pattern\n");
                int coord = moves[rand()%ind];
                playMove(coord, color);
                return coord;
            }
        }
    }
    prob = rand()%1000;
    if (prob < P_GCAPTURE) {
        for (int i = 0; i < caplen; ++i) {
            int coord = ginfo[capturable[i]].coords[0];
            if (!isSuicide(coord, color) && 
                (ko != coord || color != koColor) &&
                !isSelfAtari(coord, color)) {
                if (b[capturable[i]] == color) {
                    if (!isLadder(coord, capturable[i], color)) {
                        moves[ind++] = coord;
                    }
                }
                else moves[ind++] = coord;
            }
        }
        if (ind) {
            //fprintf(stderr, "global capture\n");
            int coord = moves[rand()%ind];
            playMove(coord, color);
            return coord;
        }
    }
    prob = rand()%1000;
    if (prob < P_GATARI) {
        for (int i = 0; i < atalen; ++i) {
            int coord = ginfo[atariable[i]].coords[0];
            bool badladder = false;
            if (!isSuicide(coord, color) && 
                (ko != coord || color != koColor) &&
                !isSelfAtari(coord, color) &&
                (!isSurrounded(coord, color) || isFalseEye(coord, color))) {
                if (b[atariable[i]] == oppositeColor[color] &&
                    canBeLadder(atariable[i], ginfo[atariable[i]].coords[1],
                    coord, oppositeColor[color])) {
                    moves[ind++] = coord;
                }
                moves[ind++] = coord;
            }
            coord = ginfo[atariable[i]].coords[1];
            if (!isSuicide(coord, color) && 
                (ko != coord || color != koColor) &&
                !isSelfAtari(coord, color) &&
                (!isSurrounded(coord, color) || isFalseEye(coord, color))) {
                if (b[atariable[i]] == oppositeColor[color] &&
                    canBeLadder(atariable[i], ginfo[atariable[i]].coords[1],
                    coord, oppositeColor[color])) {
                    moves[ind++] = coord;
                }
                moves[ind++] = coord;
            }
        }
        if (ind) {
            //fprintf(stderr, "global atari\n");
            int coord = moves[rand()%ind];
            playMove(coord, color);
            return coord;
        }
    }
    //fprintf(stderr, "randommove\n");
    return playRandomMove(color);
}

inline int GoBoard3::getClosePos(int * moves2, int color, int top, int start,
                                   char * used) {
    int end = top;
    for (int k = start; k < end; ++k) {
        for_each_neighbor(c1, moves2[k], {
            if (isEmpty(c1) && !used[c1]) {
                moves2[top++] = c1;
                used[c1] = 1;
            }
            else if (!isBorder(c1) && !used[c1]) {
                for_each_in_group(g[c1], {     // we can't use liberties because
                    for_each_neighbor(c2, c, { // we only store few of them
                        if (isEmpty(c2) && !used[c2]) {
                            moves2[top++] = c2;
                            used[c2] = 1;
                        }
                    });
                    used[c] = 1;
                });
            }
        });
    }
    return top;
}

int GoBoard3::getBonus(int c1, int color, int& wins, int& loses) {
    if (unlikely(isSuicide(c1, color) || !isLegalMove(c1, color) ||
        (isSurrounded(c1, color) && !isFalseEye(c1, color)))) {
        return FORBIDBONUS;
    }
    
    //TOO MUCH BONUS!!!!!!! 
    
    if (!isPass(lastMove) && !isResign(lastMove)) {
        char used[size2];
        memset(used, 0, size2);
        int moves2[size2 - size*4];
        moves2[0] = lastMove;
        int dist = 0;
        int top = 1;
        int last = 0;
        bool found = false;
        while (dist < 3 && !found) {
            int i = top;
            top = getClosePos(moves2, color, top, last, used);
            last = i;
            while (i < top && !found) found = (moves2[i++] == c1);
            ++dist;
        }
        if (found) wins += (2*BASEBONUS*(4-dist))/3;
    }
    
    char bonus1 = false;
    char bonus2 = false;
    
    if (unlikely(isSelfAtari(c1, color))) loses += BASEBONUS;
    else {
        memset(ladderCache, 2, sizeof(ladderCache));
        memset(patternCache, -1, sizeof(patternCache));
        if (matchSomePattern(c1, color)) bonus1 = true;
        for_each_neighbor(cn, c1, {
            if (b[cn] == oppositeColor[color] && ginfo[g[cn]].libs == 2) {
                bonus2 = true;
                int lib1 = ginfo[g[cn]].coords[0];
                if (lib1 == c1) lib1 = ginfo[g[cn]].coords[1];
                if (canBeLadder(g[cn], lib1, c1, oppositeColor[color])) {
                    wins += BASEBONUS;
                }
            }
            else if (b[cn] == color) {
                if (ginfo[g[cn]].libs == 1) {
                    if (isLadder(c1, g[cn], color)) loses += BASEBONUS;
                    else bonus1 = true;
                }
                else if (ginfo[g[cn]].libs == 2) bonus2 = true;
            }
        });
    }
    
    for_each_neighbor(cn, c1, {
        if (b[cn] == oppositeColor[color] && ginfo[g[cn]].libs == 1) {
            bonus1 = true;
        }
    });
    
    if (bonus1) wins += BASEBONUS;
    else if (bonus2) wins += BASEBONUS;
    // bad to first line
    return wins;
}

inline bool GoBoard3::repeatHash() {
    int i = 0;
    while(i < hashlen && hashes[i] != hashact) ++i;
    return i != hashlen;
}

float GoBoard3::playRandomGame(int color, double dynkomi) {
    /*if (isLadder(60, BLACK)) {
        fprintf(stderr, "%i,%i\n", 60/size, 60%size);
    }*/
    //fprintf(stderr, "startgame\n");
    while((!isPass(lastMove) || !isPass(lastMove2)) && !superko) {
        //fprintf(stderr, "turnhour: %i\n", turns);
        int mov = playHeuristicMove(color + 1);
        //printBoard();
        //fprintf(stderr, "%i,%i\n", mov/size, mov%size);
        //printBoard();
        color = !color;
        //fprintf(stderr, "%i,%i\n", mov/size, mov%size);
        //printBoard();
        //printGroups();
        //printStructures();
        //if (turns > 300) fprintf(stderr, "turns\n");
    }
    return getScore(dynkomi);
}

inline bool GoBoard3::matchSomePattern(int coord, int color) {
    if (patternCache[coord] >= 0) return patternCache[coord]; 
    check_pattern(coord, {
        //  BWB
        //  ...
        //  xxx
        if (unlikely(b[c1] == col1 && b[c2] == col2 && b[c3] == col1 &&
            b[c4] == EMPTY && b[c5] == EMPTY)) {
            //fprintf(stderr, "Hane 1\n");
            patternCache[coord] = 1;
            return true;
        }
            
        //  BW.  1st no atari
        //  ...
        //  x.x
        if (unlikely(b[c1] == col1 && ginfo[g[c1]].libs > 1 && b[c2] == col2 &&
            b[c3] == EMPTY && b[c4] == EMPTY && b[c5] == EMPTY &&
            b[c7] == EMPTY)) {
            //fprintf(stderr, "Hane 2\n");
            patternCache[coord] = 1;
            return true;
        }

        //  BWW
        //  .B.
        //  x.x
        if (unlikely(b[c1] == col1 && b[c2] == col2 && b[c3] == col2 &&
            b[c4] == EMPTY && b[c5] == EMPTY && b[c7] == EMPTY &&
            color == col1)) {
            //fprintf(stderr, "Hane 3\n");
            patternCache[coord] = 1;
            return true;
        }
        
        //  BWx
        //  B..
        //  b.x
        if (unlikely(b[c1] == col1 && b[c2] == col2 && b[c4] == col1 &&
            b[c5] == EMPTY && b[c6] != col1 && b[c7] == EMPTY)) {
            //fprintf(stderr, "Hane 4\n");
            patternCache[coord] = 1;
            return true;
        }
        
        //  .W. // no atari
        //  B.. // no atari
        //  ...
        if (unlikely(b[c1] == EMPTY && b[c2] == col2 && b[c3] == EMPTY &&
            b[c4] == col1 && b[c5] == EMPTY && b[c6] == EMPTY &&
            b[c7] == EMPTY && b[c8] == EMPTY &&
            ginfo[g[c2]].libs > 1 && ginfo[g[c4]].libs > 1)) {
            //fprintf(stderr, "Generic\n");
            patternCache[coord] = 1;
            return true;
        }
        
        //  BWx             BWx  BWx
        //  W.x   and not   W.W  W..
        //  xxx             x.x  xWx
        if (unlikely(b[c1] == col1 && b[c2] == col2 && b[c4] == col2 &&
            b[c5] != col2 && b[c7] != col2)) {
            //fprintf(stderr, "Cut1\n");
            patternCache[coord] = 1;
            return true;
        }
        
        //  BWx
        //  W.B
        //  xxx
        if (unlikely(b[c1] == col1 && b[c2] == col2 && b[c4] == col2 &&
            b[c5] == col1)) {
            //fprintf(stderr, "Cut1\n");
            patternCache[coord] = 1;
            return true;
        }
        
        //  xBx
        //  W.W
        //  www (not white)
        if (unlikely(b[c2] == col1 && b[c4] == col2 && b[c5] == col2 &&
            b[c6] != col2 && b[c7] != col2 && b[c8] != col2)) {
            //fprintf(stderr, "Cut21\n");
            patternCache[coord] = 1;
            return true;
        }
        
        //  WBx
        //  w.W
        //  xxx
        if (unlikely(b[c1] == col2 && b[c2] == col1 && b[c4] != col2 &&
            b[c5] == col2)) {
            //fprintf(stderr, "Cut22\n");
            patternCache[coord] = 1;
            return true;
        }
        
        //  B.x
        //  W.x
        //  --x (border)
        if (unlikely(b[c1] == col1 && b[c2] == EMPTY && b[c4] == col2 &&
            b[c6] == BORDER && b[c7] == BORDER)) {
            //fprintf(stderr, "Side 1\n");
            patternCache[coord] = 1;
            return true;
        }
           
        //  xBx
        //  b.W
        //  ---
        if (unlikely(b[c2] == col1 && b[c4] != col1 && b[c5] == col2 &&
            b[c6] == BORDER && b[c7] == BORDER && b[c8] == BORDER)) {
            //fprintf(stderr, "Side 2\n");
            patternCache[coord] = 1;
            return true;
        }
            
        //  xBW  w no atari
        //  b.b
        //  ---
        if (unlikely(b[c2] == col1 && b[c3] == col2 && ginfo[g[c3]].libs > 1 &&
            b[c4] != col1 && b[c5] != col1 &&
            b[c6] == BORDER && b[c7] == BORDER && b[c8] == BORDER)) {
            //fprintf(stderr, "Side 3\n");
            patternCache[coord] = 1;
            return true;
        }
          
        //  xWB
        //  xBw
        //  ---
        if (unlikely(b[c2] == col2 && b[c3] == col1 && color == col1 &&
            b[c5] != col2 && b[c6] == BORDER && b[c7] == BORDER &&
            b[c8] == BORDER)) {
            //fprintf(stderr, "Side 4\n");
            patternCache[coord] = 1;
            return true;
        }
           
        //  xWB    // b no atari
        //  B.W    // b no atari
        //  ---
        if (unlikely(b[c2] == col2 && b[c3] == col1 && ginfo[g[c3]].libs > 1 &&
            b[c4] == col1 && ginfo[g[c4]].libs > 1 && b[c5] == col2 &&
            b[c6] == BORDER && b[c7] == BORDER && b[c8] == BORDER)) {
            //fprintf(stderr, "Side 5\n");
            patternCache[coord] = 1;
            return true;
        }
        
        //  xB.
        //  W.B    // w no atari
        //  ---
        if (unlikely(b[c2] == col1 && b[c3] == EMPTY &&
            b[c4] == col2 && ginfo[g[c4]].libs > 1 && b[c5] == col1 &&
            b[c6] == BORDER && b[c7] == BORDER && b[c8] == BORDER)) {
            //fprintf(stderr, "Side 6\n");
            patternCache[coord] = 1;
            return true;
        }
    });
    patternCache[coord] = 0;
    return false;
}

void GoBoard3::printBoard() {
    char c = 0;
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j, ++c) {
            if (isBlack(c)) fprintf(stderr, "X");
            else if (isWhite(c)) fprintf(stderr, "O");
            else if (ko == c) {
                if (koColor == BLACK) fprintf(stderr, "b");
                else if (koColor == WHITE) fprintf(stderr, "w");
                else fprintf(stderr, "e");
            }
            else if (isEmpty(c)) fprintf(stderr, ".");
            else if (isBorder(c)) fprintf(stderr, "-");
        }
        fprintf(stderr, "\n");
    }
}

void GoBoard3::printGroups() {
    fprintf(stderr, "Groups:\n");
    char seen[size2];
    for (int i = 0; i < size2; ++i) seen[i] = 0;
    for (int i = 0; i < size2; ++i) {
        if (g[i] && !seen[g[i]]) {
            if (isBlack(i)) fprintf(stderr, "Black: ");
            else fprintf(stderr, "White: ");
            int cont = 1;
            int act = g[i];
            do {
                fprintf(stderr, "%i,%i ", act/size, act%size);
                act = p[act];
                cont++;
            } while (act);
            fprintf(stderr, "Size:%i\nLibs: ", cont-1);
            act = 0;
            for (act = 0; act < ginfo[g[i]].libs; ++act) {
                int x = ginfo[g[i]].coords[act];
                fprintf(stderr, "%i,%i ", x/size, x%size);
            }
            fprintf(stderr, "\n");
            seen[g[i]] = 1;
        }
    }
    fprintf(stderr, "\n");
}

void GoBoard3::printNsColor() {
    for_each_point({
        int c = cp;
        fprintf(stderr, "%i,%i - Empty:%i Black:%i White:%i, Border:%i\n",
               c/size, c%size, nscolor[c].col[EMPTY], nscolor[c].col[BLACK],
               nscolor[c].col[WHITE], nscolor[c].col[BORDER]);
    });
}

void GoBoard3::printStructures() {
    fprintf(stderr, "g: ");
    for (int i = 0; i < size2; ++i) fprintf(stderr, "%i ", g[i]);
    fprintf(stderr, "\np: ");
    for (int i = 0; i < size2; ++i) fprintf(stderr, "%i ", p[i]);
    fprintf(stderr, "\nf: ");
    for (int i = 0; i < size2; ++i) fprintf(stderr, "%i,%i ", f[i]/size, f[i]%size);
    fprintf(stderr, "\n");
    
}

bool GoBoard3::checkNeighbors() {
    for (int i = 1; i < size-1; ++i) {
        for (int j = 1; j < size-1; ++j) {
            int coord = i*size + j;
            char count[4] = { 0, 0, 0, 0 };
            for_each_neighbor(c, coord, {
                count[b[c]]++;
            });
            if (count[0] != nscolor[coord].col[0] ||
                count[1] != nscolor[coord].col[1] ||
                count[2] != nscolor[coord].col[2] ||
                count[3] != nscolor[coord].col[3]) {
                fprintf(stderr, "error %i,%i", i, j);
                return false;
            }
            
        }
    }
    return true;
}
