/* 
 * File:   UCTEngine.cpp
 * Author: Red
 * 
 * Created on 24 de marzo de 2013, 23:21
 */

#include "UCTEngine.h"

UCTEngine::UCTEngine() {
}

UCTEngine::UCTEngine(const UCTEngine& orig) {
}

UCTEngine::~UCTEngine() {
}

GoMove UCTEngine::nextMove(GoBoard2& board, char color) {
    int size = board.SIZE;
    GoBoard2* b2 = new GoBoard2(board);
    UCTTree tree(0, b2, 0, 0, (color+1)%2, 0.5, 1, 2);
    tree.expandNode();
    int it = 3000;
    while(it--) {
        bool col = color;
        UCTTree* cont = &tree;
        int bestm;
        while (cont->sons.size() > 0) {
            bestm = cont->getBest();
            cont = cont->sons[bestm];
            col = !col;
        }
        b2 = new GoBoard2(*(cont->state));
        int res = b2->randomGameUCT(col);
        cont->updateNode(res);
        delete b2;
        //tree.printTree(0);
    }
    //tree.printTree(0);
    int ind = tree.getBest();
    //std::ofstream flog;
    //flog.open("log.txt", std::ios::app);
    //flog << tree.sons[ind]->i << ' ' << tree.sons[ind]->j << std::endl;
    GoMove mov(tree.sons[ind]->i, tree.sons[ind]->j, color, true);
    if (tree.sons[ind]->value < 0.2) mov.valid = false;
    return mov;
}
