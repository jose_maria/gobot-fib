/* 
 * File:   GoEngine.h
 * Author: Red
 *
 * Created on 19 de octubre de 2012, 23:21
 */

#ifndef GOENGINE_H
#define	GOENGINE_H

#include <vector>
#include "GoBoard2.h"
#include "GoMove.h"

typedef std::vector<GoMove> VecGoMove;

class GoEngine {
public:
    
    GoEngine();
    GoEngine(const GoEngine& orig);
    virtual ~GoEngine();
    virtual GoMove nextMove(GoBoard2& board, char color);
    
protected:
    
};

#endif	/* GOENGINE_H */

