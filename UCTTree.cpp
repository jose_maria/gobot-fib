/* 
 * File:   UCTTree.cpp
 * Author: Red
 * 
 * Created on 25 de marzo de 2013, 21:04
 */

#include "UCTTree.h"
#include <fstream>

UCTTree::UCTTree() {
    sons = std::vector<UCTTree*>(0);
}

UCTTree::UCTTree(const UCTTree& orig) {
}

UCTTree::~UCTTree() {
    for (int k = 0; k < sons.size(); ++k) {
        delete sons[k];
    }
    delete state;
}

UCTTree::UCTTree(UCTTree* dad2, GoBoard2 * s, int i2, int j2,
                 int col2, double v, int w, int t) {
    i = i2;
    j = j2;
    dad = dad2;
    col = col2;
    win = w;
    tot = t;
    value = v;
    state = s;
    sons = std::vector<UCTTree*>(0);
}

int UCTTree::getBest() {
    double best = -1;
    int ind = -1;
    double lg = log(tot);
    for (int k = 0; k < sons.size(); ++k) {
        if (sons[k]->tot == 0) {
            ind = k;
            best = FPU;
        }
        else {
            double act = sons[k]->value + P*sqrt(lg/sons[k]->tot);
            if (act > best) {
                best = act;
                ind = k;
            }
        }
    }
    //std::ofstream flog;
    //flog.open("log.txt", std::ios::app);
    //flog << lg << ' ' << best << ' ' << ind << std::endl;
    return ind;
}

void UCTTree::updateNode(int res) {
    //std::cout << "imupdating" << std::endl;
    tot++;
    win += res;
    if (tot > 1) {
        value += (res - value)/tot;
        if (tot > 5) expandNode();
    }
    else value = res;
    UCTTree * act = dad;
    bool res2 = !res;
    while (act) {
        act->tot++;
        act->win += res2;
        act->value += (res - act->value)/act->tot;
        act = act->dad;
        res2 = !res2;
    }
}

void UCTTree::expandNode() {
    int size = state->SIZE;
    VecI moves = state->getAllMoves(!col);
    for (int k = 0; k < moves.size(); k++) {
        int m = moves[k];
        GoBoard2 * b2 = new GoBoard2(*(state));
        GoMove mov;
        UCTTree *tr;
        if (moves[k] >= 0) {
            mov = GoMove(moves[k]/size, moves[k]%size, !col, true);
            b2->nextMove(mov);
            tr = new UCTTree(this, b2, m/size, m%size, !col, 0, 0, 0);
        }
        else {
            mov = GoMove(-1, -1, !col, true);
            b2->nextMove(mov);
            tr = new UCTTree(this, b2, -1, -1, !col, 0, 0, 0);
        }
        sons.push_back(tr);
    }
}

void UCTTree::printTree(int sp) {
    std::ofstream flog;
    flog.open("log.txt", std::ios::app);
    for (int k = 0; k < sp; ++k) flog << ' ';
    flog << i << ',' << j << " c:" << col << " v:" << value 
         << " w:" << win << " t:" << tot << std::endl;
    //state->printBoard();
    for (int k = 0; k < sons.size(); ++k) sons[k]->printTree(sp+2);
}
