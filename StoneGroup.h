/* 
 * File:   StoneGroup.h
 * Author: Red
 *
 * Created on 21 de octubre de 2012, 22:23
 */

#ifndef STONEGROUP_H
#define	STONEGROUP_H

#include <set>

class BoardPos;

typedef std::set<BoardPos*> setS;

class StoneGroup {
public:
    StoneGroup();
    StoneGroup(BoardPos * pos, char c);
    StoneGroup(const StoneGroup& orig);
    virtual ~StoneGroup();
    void addStone(BoardPos* pos);
    
    char color;
    int num;
    setS stones;
    setS liberties;

private:

};

#endif	/* STONEGROUP_H */

