/* 
 * File:   UCTEngine3.h
 * Author: Red
 *
 * Created on 28 de abril de 2013, 20:42
 */

#ifndef UCTENGINE3_H
#define	UCTENGINE3_H

#include <math.h>
#include "GoEngine3.h"

#define P_3 0.1
#define FPU_3 1

struct uctNode {
    double value;
    int win;
    int tot;
    double vvalue;
    int vwin;
    int vtot;
    uctNode * dad;
    uctNode * sons;
};

class UCTEngine3 : public GoEngine3 {
    
public:
    UCTEngine3();
    UCTEngine3(const UCTEngine3& orig);
    virtual ~UCTEngine3();
    virtual GoMove nextMove(GoBoard3& board, char color);
    void playMove(int coord);
    void resetGame();
    
private:

    int size, size2;
    uctNode * baseTree;
    bool resetTree;
    
    inline uctNode* getBestMove(uctNode * tree, GoBoard3& b, char& col);
    inline void expandNode(uctNode * tree, GoBoard3& board, char color);
    inline void updateSon(uctNode * tree, char col, char ind, float score);
    inline int getBestSon(uctNode * tree, GoBoard3& board);
    inline int getBestMove(uctNode * tree);
    void nextTurnTree(int coord);
    void eraseSons(uctNode * tree);
    void printTree(int mov, uctNode * tree, int spaces);
};

#endif	/* UCTENGINE3_H */

