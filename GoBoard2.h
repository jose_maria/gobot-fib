/* 
 * File:   GoBoard2.h
 * Author: Red
 *
 * Created on 22 de marzo de 2013, 19:16
 */

#ifndef GOBOARD2_H
#define	GOBOARD2_H

#include <vector>
#include <set>
#include <iostream>
#include <fstream>
#include "BoardPos.h"
#include "GoMove.h"
#include "StoneGroup.h"
#define DEBUG 0
#define P_1LIB 900
#define P_2LIB 750
#define P_PATTERN 800


typedef std::vector<BoardPos> Vec;
typedef std::vector<Vec> Mat;
typedef std::set<StoneGroup*> setG;
typedef std::set<BoardPos*> setP;
typedef setP::iterator It;
typedef setG::iterator ItG;

// We use it for hashing, index:
//      black = 0
//      white = 1
//      empty = 2
typedef std::vector<int> VChar;
typedef std::vector<VChar> MChar;
typedef std::vector<MChar> VZob;
typedef std::set<int> setI;
typedef setI::iterator ItI;

class GoBoard2 {
public:
    GoBoard2();
    GoBoard2(char x);
    GoBoard2(const GoBoard2& orig);
    virtual ~GoBoard2();

    char SIZE;
    int SIZE2;
    int capWhite;
    int capBlack;
    int lastMove;
    int totturn;
    double scoreWhite;
    double scoreBlack;
    double komi;
    Mat board;
    setG bGroups;
    setG wGroups;
    setI setKo;
    setI freeMoves;
    
    void clearBoard();
    void setKomi(double);
    void printBoard();
    void printGroups();
    void printScore();
    void printMoves();
    bool nextMove(GoMove&);
    void computeScore();
    bool isLegal(char, char, char);
    bool isEye(char, char, char);
    char randomGameUCT(char);
    char randomGameMCTS(int, char);
    int getRandomMove(char);
    VChar getAllMoves(char);
    
private:
    
    VZob zob;
    int hashAct;
    setI hashes;
    char numLiber(char, char, char);
    StoneGroup* createGroup(char, char, char);
    void fusionGroups(StoneGroup*, StoneGroup*);
    void addToGroups(char, char, char);
    void killAdjacent(char, char, char);
    bool killGroup(StoneGroup*); //return possible ko
    void killStone(BoardPos*);
    bool isSuicide(char, char, char);
    int countArea(char, char, char);
    void initZob();
    int hashZob();
};

#endif	/* GOBOARD2_H */

