/* 
 * File:   RAVEEngine.h
 * Author: Red
 *
 * Created on 4 de mayo de 2013, 23:59
 */

#ifndef RAVEENGINE_H
#define	RAVEENGINE_H

#include <math.h>
#include "GoEngine3.h"

#define P_R3 0.07
#define FPU_R3 1
#define BETA 0.4

#define INITSIM 8
#define EXPANDSIM 6
#define EQUIVSIM 1800
#define RAVEWEIGHT 3

#define PASSITER 5000
#define ITER 30000
#define MAXITER 2*ITER
#define MULITER 1000
#define TURNITER 15

#define REDZONE 0.45
#define GREENZONE 0.55
#define MINKOMI 0
#define MAXKOMI 15
#define UPDATEKOMI 4000
#define STARTDYN 8
#define INCKOMI 1
#define RESETRATCHET 10

struct raveNode {
    char expand;
    double avalue;
    int awin;
    int atot;
    double value;
    int win;
    int tot;
    double vvalue;
    int vwin;
    int vtot;
    raveNode * dad;
    raveNode * sons;
};

class RAVEEngine : public GoEngine3 {
    
public:
    RAVEEngine();
    RAVEEngine(const RAVEEngine& orig);
    virtual ~RAVEEngine();
    virtual GoMove nextMove(GoBoard3& board, char color);
    void playMove(int coord);
    void resetGame();
    
    double * alive;
    double * dead;
    char hasfinalstate;
    
private:

    int size, size2;
    raveNode * baseTree;
    bool resetTree;
    double dynkomi, ratchet;
    int mulkomi;
    
    inline raveNode* getBestMove(raveNode * tree, GoBoard3& b, char& col);
    inline void expandNode(raveNode * tree, GoBoard3& board, char color);
    inline void updateSon(raveNode * tree, char col, float score,
                           GoBoard3& board, int turn);
    inline int getBestSon(raveNode * tree, GoBoard3& board);
    inline int getBestMove(raveNode * tree);
    void nextTurnTree(int coord);
    void eraseSons(raveNode * tree);
    void printTree(int mov, raveNode * tree, int spaces);
};

#endif	/* RAVEENGINE_H */

