/* 
 * File:   GoBoard.h
 * Author: Red
 *
 * Created on 19 de octubre de 2012, 23:27
 */

#ifndef GOBOARD_H
#define	GOBOARD_H

#include <vector>
#include <set>
#include <iostream>
#include <fstream>
#include "BoardPos.h"
#include "GoMove.h"
#include "StoneGroup.h"
#define DEBUG 0

typedef std::vector<BoardPos> Vec;
typedef std::vector<Vec> Mat;
typedef std::set<StoneGroup*> setG;
typedef std::set<BoardPos*> setP;
typedef setP::iterator It;
typedef setG::iterator ItG;

// We use it for hashing, index:
//      black = 0
//      white = 1
//      empty = 2
typedef std::vector<int> VChar;
typedef std::vector<VChar> MChar;
typedef std::vector<MChar> VZob;
typedef std::set<int> setI;
typedef setI::iterator ItI;

class GoBoard {
public:
    GoBoard();
    GoBoard(char x);
    GoBoard(const GoBoard& orig);
    virtual ~GoBoard();

    char SIZE;
    int capWhite;
    int capBlack;
    double scoreWhite;
    double scoreBlack;
    double komi;
    Mat board;
    setG bGroups;
    setG wGroups;
    setP setKo;
    setI movesBlack;
    setI movesWhite;
    
    void clearBoard();
    void setKomi(double);
    void printBoard();
    void printGroups();
    void printScore();
    void printMoves();
    bool nextMove(GoMove&);
    void computeScore();
    bool isLegal(char, char, char);
    bool isEye(char, char, char);
    char randomGame(int, char);
    
private:
    
    VZob zob;
    int hashAct;
    setI hashes;
    char numLiber(char, char, char);
    StoneGroup* createGroup(char, char, char);
    void fusionGroups(StoneGroup*, StoneGroup*);
    void addToGroups(char, char, char);
    void killAdjacent(char, char, char);
    void updateMovesBlack(char, char);
    void updateMovesWhite(char, char);
    bool killGroup(StoneGroup*); //return possible ko
    void killStone(BoardPos*);
    bool isSuicide(char, char, char);
    bool isAtari1(char, char, char);
    int countArea(char, char, char);
    void initZob();
    int hashZob();
};

#endif	/* GOBOARD_H */

