/* 
 * File:   MCTSEngine3.h
 * Author: Red
 *
 * Created on 18 de abril de 2013, 18:03
 */

#ifndef MCTSENGINE3_H
#define	MCTSENGINE3_H

#include "GoEngine3.h"

struct mctsNode {
    int win;
    int tot;
    mctsNode * dad;
    mctsNode * brother;
    mctsNode * sons;
};

class MCTSEngine3 : public GoEngine3 {
    
public:
    MCTSEngine3();
    MCTSEngine3(const MCTSEngine3& orig);
    virtual ~MCTSEngine3();
    virtual GoMove nextMove(GoBoard3& board, char color);
    
private:

    int size, size2;
    
    inline mctsNode* getBestMove(mctsNode * tree, GoBoard3& b, char& col);
    inline void expandNode(mctsNode * tree, GoBoard3& board, char color);
    inline void updateSon(mctsNode * tree, GoBoard3& board, char col,
                           char ind, float score);
    inline int getBestSon(mctsNode * tree, GoBoard3& board);
    void eraseSons(mctsNode * tree);
    void printTree(int mov, mctsNode * tree, int spaces);
};


#endif	/* MCTSENGINE3_H */

