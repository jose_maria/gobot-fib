/* 
 * File:   GoMove.h
 * Author: Red
 *
 * Created on 19 de octubre de 2012, 23:31
 */

#ifndef GOMOVE_H
#define	GOMOVE_H

class GoMove {
public:
    GoMove();
    GoMove(char i2, char j2, char type2, bool b);
    GoMove(const GoMove& orig);
    virtual ~GoMove();
    
    char i;
    char j;
    char type; // 0 black, 1 white
    char valid;
    
private:

};

#endif	/* GOMOVE_H */

