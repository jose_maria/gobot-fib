/* 
 * File:   BoardPos.h
 * Author: Red
 *
 * Created on 19 de octubre de 2012, 23:50
 */

#ifndef BOARDPOS_H
#define	BOARDPOS_H

#include "StoneGroup.h"

class BoardPos {
public:
    BoardPos();
    BoardPos(const BoardPos& orig);
    virtual ~BoardPos();
    
    bool isWhite2();
    bool isBlack2();
    bool isEmpty2();
    bool koMark();
    bool hasGroup();
    
    char type; // 0 black, 1 white, 7 ko opponent turn, 8 start of ko, 9 empty
    char i;
    char j;
    StoneGroup * group;
    
private:

};

#endif	/* BOARDPOS_H */

