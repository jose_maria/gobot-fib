/* 
 * File:   EngineIO.h
 * Author: Red
 *
 * Created on 22 de octubre de 2012, 22:04
 */

#ifndef ENGINEIO_H
#define	ENGINEIO_H

#include "GoBoard.h"
#include "GoMove.h"

class EngineIO {
public:
    EngineIO();
    EngineIO(char c);
    EngineIO(const EngineIO& orig);
    virtual ~EngineIO();
    GoMove nextMove(GoBoard& board);
    
    char color;
private:

};

#endif	/* ENGINEIO_H */

