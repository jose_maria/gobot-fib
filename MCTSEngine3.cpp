/* 
 * File:   MCTSEngine3.cpp
 * Author: Red
 * 
 * Created on 18 de abril de 2013, 18:03
 */

#include "MCTSEngine3.h"

MCTSEngine3::MCTSEngine3() {
}

MCTSEngine3::MCTSEngine3(const MCTSEngine3& orig) {
}

MCTSEngine3::~MCTSEngine3() {
}

void MCTSEngine3::printTree(int mov, mctsNode * tree, int spaces) {
    if (tree->tot != 0) {
        for (int i = 0; i < spaces; ++i) fprintf(stderr, " ");
        fprintf(stderr, "%i,%i - %i %i\n", mov/size, mov%size,
                                           tree->win, tree->tot);
    }
    if (tree->sons) {
        for (int i = size; i < size2 - size; ++i) {
            printTree(i, &tree->sons[i], spaces+2);
        }
    }
}

inline mctsNode* MCTSEngine3::getBestMove(mctsNode * tree, GoBoard3& b,
                                           char& col) {
    //GoBoard3 orig = GoBoard3(b);
    while(tree->sons) {
        int mov = getBestSon(tree, b);
        char valid = b.playMove(mov, col + 1);
        col = !col;
        if (isPass(mov)) return tree;
        tree = &tree->sons[mov];
    }
    return tree;    
}

inline void MCTSEngine3::expandNode(mctsNode * tree, GoBoard3& board, 
                                     char color) {
    int memsize = size2 * sizeof(*(tree->sons));
    char* x = (char*)malloc(memsize);
    tree->sons = (mctsNode*)x;
    memset(tree->sons, 0, memsize);
    for (int i = 0; i < size2; ++i) tree->sons[i].dad = tree;
    for (int i = 0; i < board.flen; ++i) {
        int c = board.f[i];
        int wins = 0;
        int loses = 0;
        int bonus = board.getBonus(c, color + 1, wins, loses);
        if (bonus == FORBIDBONUS) tree->sons[c].tot = -1;
        else {
            tree->sons[c].win += wins;
            tree->sons[c].tot += wins+loses;
        }
    }
    // should we propagate the bonus to the parents?
}

void MCTSEngine3::eraseSons(mctsNode * tree) {
    if (tree->sons) {
        for (int i = 0; i < size2; ++i) {
            eraseSons(&tree->sons[i]);
        }
        free(tree->sons);
    }
}

inline int MCTSEngine3::getBestSon(mctsNode * tree, GoBoard3& board) {
    double best = -1;
    int mov = -1;
    int besttot = -1;
    for (int i = 0; i < board.flen; ++i) {
        int ii = board.f[i];
        float res = -1;
        if (tree->sons[ii].tot == 0) res = 0.49;
        else if (tree->sons[ii].tot > 0) {
            res = double(tree->sons[ii].win)/tree->sons[ii].tot;
        }
        if (res > best || (res == best && besttot > tree->sons[ii].tot)) {
            best = res;
            besttot = tree->sons[ii].tot;
            mov = ii;
        }
    }
    return mov;
}

inline void MCTSEngine3::updateSon(mctsNode * tree, GoBoard3& board,
                                    char col, char ind, float score) {
    //fprintf(stderr, "score: %f\n", score);
    char addWin = !col;
    if (score > 0) addWin = col;
    tree->tot++;
    tree->win += addWin;
    addWin = !addWin;
    while(tree->dad) {
        tree->dad->tot++;
        tree->dad->win += addWin;
        tree = tree->dad;
        addWin = !addWin;
    }
}

GoMove MCTSEngine3::nextMove(GoBoard3& board, char color) {
    size = board.size;
    size2 = board.size2;
    color = (color == WHITE);
    
    mctsNode * baseTree;
    char* x = (char*)malloc(sizeof(*baseTree));
    baseTree = (mctsNode*)x;
    memset(baseTree, 0, sizeof(*baseTree));
    expandNode(baseTree, board, color);
    for (int i = size; i < size2 - size; ++i) {
        if (baseTree->sons[i].tot > 0) {
            fprintf(stderr, "%i,%i - %i %i\n", i/size, i%size,
                                  baseTree->sons[i].win, baseTree->sons[i].tot);
        }
    
    }
    int it = 30000;
    GoBoard3 b2 = GoBoard3(board);
    while(it) {
        char col = color;
        mctsNode * bestMove = getBestMove(baseTree, b2, col);
        if (bestMove->tot > 3 && !bestMove->sons) {
            expandNode(bestMove, b2, !col);
        }
        
        if (isPass(b2.lastMove)) {
            float score = b2.playRandomGame(!col, 0);
            updateSon(bestMove, b2, col, PASS, score);
            --it;
        }
        else {
            char ind = b2.playHeuristicMove(col + 1);
            if (isPass(ind) || (ind > size && b2.g[ind])) {
                float score = b2.playRandomGame(!col, 0);
                updateSon(bestMove, b2, !col, ind, score);
                //b2.printBoard();
                //fprintf(stderr, "score: %f\n", score);
                //printTree(0, baseTree, 0);
                it--;
            }
        }
        //printTree(0, baseTree, 0);
        b2.resetStateBoard(board);
    }
    int mov = getBestSon(baseTree, board);
    board.printBoard();
    for (int i = size; i < size2 - size; ++i) {
        if (baseTree->sons[i].tot > 0) {
            fprintf(stderr, "%i,%i - %i %i\n", i/size, i%size,
                                  baseTree->sons[i].win, baseTree->sons[i].tot);
        }
    
    }
    fprintf(stderr, "\n\n");
    eraseSons(baseTree);
    free(baseTree);
    if (!isPass(mov) && !isResign(mov)) {
        return GoMove(mov/size - 1, mov%size - 1, color, true);
    }
    else if (isPass(mov)) {
        return GoMove(-1, -1, color, true);
    }
    else return GoMove(-1, -1, color, false);
}
