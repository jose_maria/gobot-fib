/* 
 * File:   MCTSTree.cpp
 * Author: Red
 * 
 * Created on 20 de marzo de 2013, 20:20
 */

#include "MCTSTree.h"
#include <fstream>

MCTSTree::MCTSTree() {
    sons = std::vector<MCTSTree*>(0);
}

MCTSTree::MCTSTree(const MCTSTree& orig) {
}

MCTSTree::~MCTSTree() {
    for (int k = 0; k < sons.size(); ++k) {
        delete sons[k];
    }
}

MCTSTree::MCTSTree(MCTSTree* dad2, int i2, int j2, int col2, double v, int w, int t) {
    dad = dad2;
    i = i2;
    j = j2;
    col = col2;
    win = w;
    tot = t;
    value = v;
    sons = std::vector<MCTSTree*>(0);
}

int MCTSTree::getBest() {
    double best = -1;
    int ind = -1;
    for (int k = 0; k < sons.size(); ++k) {
        if (sons[k]->value > best) {
            best = sons[k]->value;
            ind = k;
        }
    }
    return ind;
}

void MCTSTree::updateSon(int i2, int j2, int res) {
    bool nofound = true;
    //std::cout << "imupdating" << std::endl;
    for (int k = 0; k < sons.size() && nofound; ++k) {
        if (sons[k]->i == i2 && sons[k]->j == j2) {
            sons[k]->tot++;
            sons[k]->win += res;
            sons[k]->value += (res - sons[k]->value)/sons[k]->tot;
            nofound = false;
        }
    }
    if (nofound) {
        MCTSTree *tree = new MCTSTree(this, i2, j2, (col+1)%2, res, res, 1);
        sons.push_back(tree);
    }
    MCTSTree * act = this;
    while (act) {
        res = (res+1)%2;
        act->tot++;
        act->win += res;
        act->value += (res - act->value)/act->tot;
        act = act->dad;
    }
}

void MCTSTree::printTree(int sp) {
    std::ofstream flog;
    flog.open("log.txt", std::ios::app);
    for (int k = 0; k < sp; ++k) flog << ' ';
    flog << i << ',' << j << " c:" << col << " v:" << value 
         << " w:" << win << " t:" << tot << std::endl;
    for (int k = 0; k < sons.size(); ++k) sons[k]->printTree(sp+2);
    //flog << std::endl;
}
