/* 
 * File:   move.h
 * Author: Red
 *
 * Created on 7 de abril de 2013, 20:28
 */

#ifndef MOVE_H
#define	MOVE_H

#ifdef	__cplusplus
extern "C" {
#endif

#define PASS -1
#define RESIGN -2
#define EMPTY 0
#define BLACK 1
#define WHITE 2
#define BORDER 3
#define TYPE_MAX 4
    
    static const int oppositeColor[] = {EMPTY, WHITE, BLACK, BORDER};

#ifdef	__cplusplus
}
#endif

#endif	/* MOVE_H */

