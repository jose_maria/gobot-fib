#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=Cygwin-Windows
CND_DLIB_EXT=dll
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/UCTTree.o \
	${OBJECTDIR}/MCTSTree.o \
	${OBJECTDIR}/GoBoard3.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/UCTEngine.o \
	${OBJECTDIR}/MCTSEngine3.o \
	${OBJECTDIR}/GoBoard.o \
	${OBJECTDIR}/GoEngine.o \
	${OBJECTDIR}/GoMove.o \
	${OBJECTDIR}/MCEngine.o \
	${OBJECTDIR}/RAVEEngine.o \
	${OBJECTDIR}/StoneGroup.o \
	${OBJECTDIR}/MCEngine3.o \
	${OBJECTDIR}/GoEngine3.o \
	${OBJECTDIR}/MCTSEngine.o \
	${OBJECTDIR}/UCTEngine3.o \
	${OBJECTDIR}/EngineIO.o \
	${OBJECTDIR}/BoardPos.o \
	${OBJECTDIR}/gtpMain.o \
	${OBJECTDIR}/GoBoard2.o \
	${OBJECTDIR}/gtp.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/gobot-fib.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/gobot-fib.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/gobot-fib ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/UCTTree.o: UCTTree.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/UCTTree.o UCTTree.cpp

${OBJECTDIR}/MCTSTree.o: MCTSTree.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/MCTSTree.o MCTSTree.cpp

${OBJECTDIR}/GoBoard3.o: GoBoard3.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/GoBoard3.o GoBoard3.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/UCTEngine.o: UCTEngine.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/UCTEngine.o UCTEngine.cpp

${OBJECTDIR}/MCTSEngine3.o: MCTSEngine3.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/MCTSEngine3.o MCTSEngine3.cpp

${OBJECTDIR}/GoBoard.o: GoBoard.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/GoBoard.o GoBoard.cpp

${OBJECTDIR}/GoEngine.o: GoEngine.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/GoEngine.o GoEngine.cpp

${OBJECTDIR}/GoMove.o: GoMove.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/GoMove.o GoMove.cpp

${OBJECTDIR}/MCEngine.o: MCEngine.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/MCEngine.o MCEngine.cpp

${OBJECTDIR}/RAVEEngine.o: RAVEEngine.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/RAVEEngine.o RAVEEngine.cpp

${OBJECTDIR}/StoneGroup.o: StoneGroup.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/StoneGroup.o StoneGroup.cpp

${OBJECTDIR}/MCEngine3.o: MCEngine3.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/MCEngine3.o MCEngine3.cpp

${OBJECTDIR}/GoEngine3.o: GoEngine3.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/GoEngine3.o GoEngine3.cpp

${OBJECTDIR}/MCTSEngine.o: MCTSEngine.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/MCTSEngine.o MCTSEngine.cpp

${OBJECTDIR}/UCTEngine3.o: UCTEngine3.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/UCTEngine3.o UCTEngine3.cpp

${OBJECTDIR}/EngineIO.o: EngineIO.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/EngineIO.o EngineIO.cpp

${OBJECTDIR}/BoardPos.o: BoardPos.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/BoardPos.o BoardPos.cpp

${OBJECTDIR}/gtpMain.o: gtpMain.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/gtpMain.o gtpMain.cpp

${OBJECTDIR}/GoBoard2.o: GoBoard2.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/GoBoard2.o GoBoard2.cpp

${OBJECTDIR}/gtp.o: gtp.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.c) -O3 -MMD -MP -MF $@.d -o ${OBJECTDIR}/gtp.o gtp.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/gobot-fib.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
