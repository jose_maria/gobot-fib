/* 
 * File:   GoEngine3.cpp
 * Author: Red
 * 
 * Created on 15 de abril de 2013, 20:39
 */

#include <stdlib.h>
#include <iostream>
#include "GoEngine3.h"

typedef std::vector<int> V;

GoEngine3::GoEngine3() {
    srand(time(0));
}

GoEngine3::GoEngine3(const GoEngine3& orig) {
}

GoEngine3::~GoEngine3() {
}

GoMove GoEngine3::nextMove(GoBoard3& board, char color) {
    int mov = board.playRandomMove(color);
    if (!isPass(mov) && !isResign(mov)) {
        return GoMove(mov/board.size - 1, mov%board.size - 1, color, true);
    }
    else if (isPass(mov)) {
        return GoMove(-1, -1, color, true);
    }
    else return GoMove(-1, -1, color, false);
}
