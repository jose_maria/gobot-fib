/* 
 * File:   MCEngine.h
 * Author: Red
 *
 * Created on 16 de marzo de 2013, 1:30
 */

#ifndef MCENGINE_H
#define	MCENGINE_H

#include "GoEngine.h"
#include <algorithm>

struct mcMove {
    int coord;
    int win;
    int tot;
};

typedef std::vector<mcMove> VecM;

class MCEngine : public GoEngine {
    
    
public:
    MCEngine();
    MCEngine(const MCEngine& orig);
    virtual ~MCEngine();
    virtual GoMove nextMove(GoBoard2& board, char color);
    
private:
    
    char randomGame(int move, GoBoard2 board, char color);
    VecM getAllMoves(GoBoard2& board, char color);
};

#endif	/* MCENGINE_H */

