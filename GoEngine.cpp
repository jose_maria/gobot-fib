/* 
 * File:   GoEngine.cpp
 * Author: Red
 * 
 * Created on 19 de octubre de 2012, 23:21
 */

#include <stdlib.h>
#include <iostream>
#include "GoEngine.h"

typedef std::vector<int> V;

GoEngine::GoEngine() {
    srand(time(0));
}

GoEngine::GoEngine(const GoEngine& orig) {
}

GoEngine::~GoEngine() {
}

GoMove GoEngine::nextMove(GoBoard2& board, char color) {
    V moves(0);
    for (int i = 0; i < board.SIZE; ++i) {
        for (int j = 0; j < board.SIZE; ++j) {
            if (board.isLegal(i, j, color) and !board.isEye(i, j, color)) {
                moves.push_back(i*board.SIZE + j);
            }
        }
    }
    if (moves.size() == 0) return GoMove(-1,-1,-1, false);
    char ind = rand()%moves.size();
    return GoMove(moves[ind]/board.SIZE, moves[ind]%board.SIZE, color, true);
}

