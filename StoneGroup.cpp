/* 
 * File:   StoneGroup.cpp
 * Author: Red
 * 
 * Created on 21 de octubre de 2012, 22:23
 */

#include "StoneGroup.h"

StoneGroup::StoneGroup() {
}

StoneGroup::StoneGroup(BoardPos* pos, char c) {
    num = 0;
    color = c;
    stones.insert(pos);
}

StoneGroup::StoneGroup(const StoneGroup& orig) {
}

StoneGroup::~StoneGroup() {
}

void StoneGroup::addStone(BoardPos* pos) {
    stones.insert(pos);
    ++num;
    //act liberties
}
