/* 
 * File:   RAVEEngine.cpp
 * Author: Red
 * 
 * Created on 4 de mayo de 2013, 23:59
 */

#include "RAVEEngine.h"
#include <algorithm>

RAVEEngine::RAVEEngine() {
    dynkomi = 0;
    ratchet = MAXKOMI;
    resetTree = false;
    baseTree = 0;
    hasfinalstate = false;
}

RAVEEngine::RAVEEngine(const RAVEEngine& orig) {
}

RAVEEngine::~RAVEEngine() {
    if (baseTree) {
        eraseSons(baseTree);
        free(baseTree);
    }
}

void RAVEEngine::resetGame() {
    dynkomi = 0;
    ratchet = MAXKOMI;
    resetTree = true;
    hasfinalstate = false;
}

void RAVEEngine::playMove(int coord) {
    if (!resetTree && !isPass(coord) && baseTree && baseTree->sons) {
        nextTurnTree(coord);
    }
    else resetTree = true;
    //if (baseTree) printTree(0, baseTree, 0);
}

void RAVEEngine::nextTurnTree(int coord) {
    char* x = (char*)malloc(sizeof(*baseTree));
    raveNode * tmp = (raveNode*)x;
    tmp->atot = baseTree->sons[coord].vtot;
    tmp->awin = baseTree->sons[coord].vwin;
    tmp->avalue = baseTree->sons[coord].vvalue;
    tmp->tot = baseTree->sons[coord].tot;
    tmp->win = baseTree->sons[coord].win;
    tmp->value = baseTree->sons[coord].value;
    tmp->vtot = baseTree->sons[coord].vtot;
    tmp->vwin = baseTree->sons[coord].vwin;
    tmp->vvalue = baseTree->sons[coord].vvalue;
    tmp->sons = baseTree->sons[coord].sons;
    for (int i = 0; i < size2; ++i) {
        if (i != coord) eraseSons(&baseTree->sons[i]);
    }
    free(baseTree->sons);
    free(baseTree);
    baseTree = tmp;
    baseTree->dad = 0;
    if (baseTree->sons) {
        for (int i = 0; i < size2; ++i) baseTree->sons[i].dad = baseTree;
    }
    //printTree(0, baseTree, 0);
    
}

void RAVEEngine::printTree(int mov, raveNode * tree, int spaces) {
    if (spaces > 2) return;
    if (tree->tot > 0) {
        for (int i = 0; i < spaces; ++i) fprintf(stderr, " ");
        double res = 0;
        double beta = 0;
        if (tree->dad) {
            double uctterm = (tree->value*tree->tot + tree->vvalue*tree->vtot)/
                        (tree->tot+tree->vtot);
            double raveterm = tree->avalue;
            double explorationterm = P_R3 *
               sqrt(log(tree->dad->tot+tree->dad->vtot)/(tree->tot+tree->vtot));
            beta = tree->atot /
                          (tree->atot + (tree->tot+tree->vtot) +
                          ((double)tree->atot*(tree->tot+tree->vtot))/EQUIVSIM);
            res = uctterm*(1-beta) + raveterm*beta + explorationterm;
        }
        fprintf(stderr, "%i,%i - %i %i %f , %i %i %f , %i %i %f, %f %f\n",
                mov/size, mov%size, tree->win, tree->tot, tree->value,
                tree->awin, tree->atot, tree->avalue,
                tree->vwin, tree->vtot, tree->vvalue,
                beta, res);
    }
    if (tree->sons) {
        for (int i = size; i < size2 - size; ++i) {
            printTree(i, &tree->sons[i], spaces+2);
        }
    }
}

inline raveNode* RAVEEngine::getBestMove(raveNode * tree, GoBoard3& b,
                                          char& col) {
    GoBoard3 orig = GoBoard3(b);
    while(tree->sons) {
        int mov = getBestSon(tree, b);
        b.playMove(mov, col + 1);
        if (unlikely(b.superko)) {
            //fprintf(stderr, "superko %i,%i\n", mov/size, mov%size);
            tree->sons[mov].tot = -1;
            b.resetStateBoard(orig);
        }
        else {
            col = !col;
            if (unlikely(isPass(mov))) return tree;
            tree = &tree->sons[mov];
        }
    }
    return tree;    
}

inline void RAVEEngine::expandNode(raveNode * tree, GoBoard3& board, 
                                     char color) {
    int memsize = size2 * sizeof(*(tree->sons));
    char* x = (char*)malloc(memsize);
    tree->sons = (raveNode*)x;
    memset(tree->sons, 0, memsize);
    for (int i = 0; i < size2; ++i) {
        tree->sons[i].dad = tree;
        tree->sons[i].awin = INITSIM;
        tree->sons[i].atot = INITSIM*2;
        tree->sons[i].avalue = 0.5;
        tree->sons[i].value = 0.5;
        
    }
    
    for (int i = 0; i < board.flen; ++i) {
        int c = board.f[i];
        int wins = 0;
        int loses = 0;
        if (likely(board.getBonus(c, color + 1, wins, loses) != FORBIDBONUS)) {
            tree->sons[c].vwin = INITSIM + wins;
            tree->sons[c].vtot += INITSIM*2 + wins + loses;
            tree->sons[c].vvalue = tree->sons[c].vwin / tree->sons[c].vtot;
        }
        else tree->sons[c].tot = -1;
    }
}

void RAVEEngine::eraseSons(raveNode * tree) {
    if (tree->sons) {
        for (int i = 0; i < size2; ++i) {
            eraseSons(&tree->sons[i]);
        }
        free(tree->sons);
    }
}

inline int RAVEEngine::getBestSon(raveNode * tree, GoBoard3& board) {
    double best = 0;
    int mov = -1;
    double logtot = log(tree->tot + tree->vtot);
    double logatot = log(tree->atot);
    for (int i = 0; i < board.flen; ++i) {
        int ii = board.f[i];
        int sontot = tree->sons[ii].tot + tree->sons[ii].vtot;
        if (tree->sons[ii].tot == 0 && FPU_R3 > best) {
            best = FPU_R3;
            mov = ii;
        }
        else if (tree->sons[ii].tot > 0) {
            //fprintf(stderr, "tot:%i\n", tree->sons[ii].tot);
            double explorationterm = P_R3 * sqrt(logtot/sontot);
            double uctterm = (tree->sons[ii].value*tree->sons[ii].tot +
                         tree->sons[ii].vvalue*tree->sons[ii].vtot)/sontot +
                         explorationterm;
            double explorationaterm = 0;
            //if (tree->atot > 0) P_R3 * sqrt(logatot/tree->sons[ii].atot);
            double raveterm = tree->sons[ii].avalue + explorationaterm;
            double beta = tree->sons[ii].atot /
                          (tree->sons[ii].atot + sontot +
                          ((double)tree->sons[ii].atot*sontot)/EQUIVSIM);
            double res = uctterm*(1-beta) + raveterm*beta;
            if (res > best) {
                best = res;
                mov = ii;
            }
        }
    }
    return mov;
}

inline int RAVEEngine::getBestMove(raveNode * tree) {
    double best = 0;
    int mov = -1;
    for (int i = size; i < size2-size; ++i) {
        if (tree->sons[i].tot > best) {
            best = tree->sons[i].tot;
            mov = i;
        }
    }
    // consider pass 
    return mov;
}


inline void RAVEEngine::updateSon(raveNode * tree, char col, float score, 
                                  GoBoard3& board, int turn) {
    //fprintf(stderr, "score: %f\n", score);
    char addWin = !col;
    if (score > 0) addWin = col;
    tree->tot++;
    tree->win += addWin;
    if (tree->tot > 1) {
        if (tree->tot == EXPANDSIM) tree->expand = true;
        tree->value += (addWin - tree->value) / tree->tot;
    }
    else tree->value = addWin;
    addWin = !addWin;
    tree = tree->dad;
    //fprintf(stderr, "getBestMove\n");
    int moves[size2];
    //fprintf(stderr, "turn: %i\n", turn);
    memset(moves, -1, sizeof(moves));
    for (int i = board.turns - 1; i >= turn; i--) {
        if (!isPass(board.movs[i])) moves[board.movs[i]] = i;
    }
    /*for (int i = 0; i < board.turns; ++i) {
        int mov = board.movs[i];
        fprintf(stderr, "%i: %i,%i\n", i, mov/size, mov%size);
    }*/
    while(tree) {
        //fprintf(stderr, "dad1\n");
        tree->tot++;
        tree->win += addWin;
        tree->value += (addWin - tree->value) / tree->tot;
        //board.printBoard();
        for (int i = size; i < size2 - size; ++i) {
            /*if (moves[i] != -1) fprintf(stderr, "mov:%i col2:%i\n", moves[i], col2);
            if (moves[i] != -1 && (moves[i]%2 == 1) == col2) {
                fprintf(stderr, "%i,%i\n", i/size, i%size);
            }*/
            if (moves[i] != -1 && moves[i]%2 == col%2) {
                int weight = 1 + ((RAVEWEIGHT * turn) / (moves[i]+1));
                tree->sons[i].atot += weight;
                tree->sons[i].awin += (!addWin) * weight;
                tree->sons[i].avalue += (!addWin - tree->sons[i].avalue) *
                                         weight / tree->sons[i].atot;
            }
        }
        turn--;
        if (turn) moves[board.movs[turn]] = turn;
        tree = tree->dad;
        addWin = !addWin;
        col = !col;
    }
}

GoMove RAVEEngine::nextMove(GoBoard3& board, char color) {
    //return GoMove(-1, -1, color, true);
    size = board.size;
    size2 = board.size2;
    /*for (int i = 0; i < size2; ++i) {
        if (board.b[i] == EMPTY) {
            for_each_neighbor(cc, i, {
                int g = board.g[cc];
                if (board.b[cc] == BLACK && board.ginfo[g].libs == 1) {
                    fprintf(stderr, "enter\n");
                    if (board.isLadderp(i, g, BLACK)) {
                        fprintf(stderr, "%i,%i is Ladder\n", i/size, i%size);
                    }
                }
            });
        }
    }*/
    mulkomi = 1;
    double ratio = double(board.turns) / (size2 - size*4);
    if (color == WHITE) mulkomi = -1;
    color = (color == WHITE);
    //board.playRandomGame(color, 0);
    //return GoMove(-1, -1, color, true);
    if (resetTree || !baseTree) {
        if (baseTree) {
            eraseSons(baseTree);
            free(baseTree);
        }
        if (resetTree) resetTree = false;
        char* x = (char*)malloc(sizeof(*baseTree));
        baseTree = (raveNode*)x;
        memset(baseTree, 0, sizeof(*baseTree));
        expandNode(baseTree, board, color);
    }
    if (!baseTree->sons) {
        expandNode(baseTree, board, color);
    }
    /*for (int i = size; i < size2 - size; ++i) {
        float res = 0;
        res = baseTree->sons[i].value +
              P_R3 * sqrt(log(baseTree->tot)/baseTree->sons[i].tot);
        if (baseTree->sons[i].tot > 0) {
            fprintf(stderr, "%i,%i - %i %i %f %i %i %f %f\n", i/size, i%size,
                                  baseTree->sons[i].win, baseTree->sons[i].tot,
                                  baseTree->sons[i].value, 
                                  baseTree->sons[i].awin, baseTree->sons[i].atot,
                                  baseTree->sons[i].avalue, res);
        }
    }
    fprintf(stderr, "\n\n");*/
    int it = PASSITER;
    int countsuperko = 0;
    GoBoard3 b2 = GoBoard3(board);
    /*for (int i = 0; i < size2; ++i) {
        fprintf(stderr, "tot:%i atot:%i\n",
                baseTree->sons[i].tot, baseTree->sons[i].atot);
    }*/
    if (isPass(board.lastMove)) { // this is a very ugly fix
        alive = (double*)malloc(size2 * sizeof(*alive));
        dead = (double*)malloc(size2 * sizeof(*dead));
        for (int i = 0; i < size2; ++i) alive[i] = 0.0, dead[i] = 0.0;
        int win = 0;
        int sim = it;
        while(it) {
            float score = b2.playRandomGame(color, 0);
            if (!b2.superko) {
                if (score > 0) win += color;
                else win += !color;
                for (int i = 0; i < size2; ++i) {
                    if (board.b[i] == BLACK || board.b[i] == WHITE) {
                        if (board.b[i] == b2.b[i]) alive[i] += 1.0;
                        else dead[i] += 1.0;
                    }
                }
                it--;
            }
            //fprintf(stderr, "fin\n");
            b2.resetStateBoard(board);
        }
        //fprintf(stderr, "win:%i sim:%i\n", win, sim);
        if (double(win) / sim > 0.8) {
            hasfinalstate = true;
            for (int i = 0; i < size2; ++i) {
                alive[i] /= PASSITER;
                dead[i] /= PASSITER;
            }
            return GoMove(-1, -1, color, true);
        }
    }
    it = std::min(MAXITER, ITER + std::max(0, board.turns - TURNITER) * MULITER);
    while(it > 0) {
        for (int i = 0; i < UPDATEKOMI; ++i) {
            //fprintf(stderr, "%i\n", i);
            char col = color;
            raveNode * bestMove = getBestMove(baseTree, b2, col);
            bool ispass = false;
            if (unlikely(bestMove->sons != 0)) ispass = true;
            else if (bestMove->expand) {
                expandNode(bestMove, b2, col);
                bestMove->expand = false;
            }
            //b2.printBoard();
            int turn = b2.turns - 1;
            //fprintf(stderr, "turns: %i\n", turn);
            float score = b2.playRandomGame(col, dynkomi*mulkomi);
            if (likely(!b2.superko)) {
                //b2.printBoard();
                if (likely(!ispass)) updateSon(bestMove, !col, score, b2, turn);
                else updateSon(bestMove, col, score, b2, turn);
                it--;
            }
            else {
                bestMove->expand = true;
                countsuperko++;
            }
            b2.resetStateBoard(board);
        }
        /*if (board.turns > STARTDYN) {
            double val = 1 - baseTree->value;
            if (val < REDZONE) {
                if (dynkomi > 0) ratchet = dynkomi;
                if (dynkomi > MINKOMI) dynkomi -= INCKOMI;
            }
            else if (val > GREENZONE && dynkomi < ratchet) {
                dynkomi += INCKOMI;
            }
            if (dynkomi < 0 && ratio > 0.65) dynkomi = 0;
        }*/
        //fprintf(stderr, "komi: %f ratchet: %f val: %f\n",
        //            dynkomi*mulkomi, ratchet, 1 - baseTree->value);
        //fprintf(stderr, "komi: %f ratchet: %f val: %f\n",
        //dynkomi*mulkomi, ratchet, 1 - baseTree->value);
    }
    /*for (int i = size; i < size2-size; ++i) {
        int sontot = baseTree->sons[i].tot + baseTree->sons[i].vtot;
        float beta = baseTree->sons[i].atot /
                         double(baseTree->sons[i].atot + sontot +
                          ((double)baseTree->sons[i].atot*sontot)/EQUIVSIM);
        fprintf(stderr, "atot:%i sontot:%i beta:%f\n",
                        baseTree->sons[i].atot, sontot, beta);
    }*/
    if (ratchet < MAXKOMI) ratchet++;
    //fprintf(stderr, "%f %f\n", dynkomi, ratchet);
    //fprintf(stderr, "superko:%i\n", countsuperko);
    //fprintf(stderr, "contsuperko:%i\n", countsuperko);
    //fprintf(stderr, "getBestMoveFin\n");
    int mov = getBestMove(baseTree);
    //board.printBoard();
    /*for (int i = size; i < size2 - size; ++i) {
        int sontot = (baseTree->sons[i].tot + baseTree->sons[i].vtot);
        float logtot = log(baseTree->tot + baseTree->vtot);
        float uctterm = (baseTree->sons[i].value*baseTree->sons[i].tot +
                         baseTree->sons[i].vvalue*baseTree->sons[i].vtot)/sontot;
        float explorationterm = P_R3 * sqrt(logtot/sontot);
        float raveterm = baseTree->sons[i].avalue;
        float res = uctterm*(1-BETA) + raveterm*BETA + explorationterm;
        if (baseTree->sons[i].tot > 0) {
            fprintf(stderr, "%i,%i - %i %i %f , %i %i %f , %f %f\n",
                                  i/size, i%size,
                                  baseTree->sons[i].win, baseTree->sons[i].tot,
                                  baseTree->sons[i].value, 
                                  baseTree->sons[i].awin, baseTree->sons[i].atot,
                                  baseTree->sons[i].avalue, uctterm, res);
        }
    }
    fprintf(stderr, "\n\n");*/
    //printTree(0, baseTree, 0);
    if (mov != PASS && baseTree->sons[mov].value < 0.2) mov = RESIGN;
    if (!isPass(mov) && !isResign(mov)) {
        nextTurnTree(mov);
        return GoMove(mov/size - 1, mov%size - 1, color, true);
    }
    else if (isPass(mov)) {
        resetTree = true;
        return GoMove(-1, -1, color, true);
    }
    else {
        resetTree = true;
        return GoMove(-1, -1, color, false);
    }
}

